package dominion.communication.util;

public class ThreadUtilities {
	public static void sleep(int millis) {
		long time = System.currentTimeMillis();
		while (System.currentTimeMillis() - time < millis) {
			try {
				Thread.sleep(millis - (System.currentTimeMillis() - time));
			} catch (InterruptedException e) {
			}
		}
	}
}
