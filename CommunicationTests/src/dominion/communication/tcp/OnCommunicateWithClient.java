package dominion.communication.tcp;

import java.io.PrintWriter;
import java.util.Scanner;

public interface OnCommunicateWithClient {
	public void communicateWithClient(Scanner in, PrintWriter out);
}
