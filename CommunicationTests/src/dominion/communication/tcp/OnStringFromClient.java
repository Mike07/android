package dominion.communication.tcp;

public interface OnStringFromClient {
	public void onStringFromClient(String newString);
}
