package dominion.communication.tcp;

import dominion.communication.tcp.client.ClientThread;

public interface OnCommunicationOnClientClosed {
	/**
	 * Wird aufgerufen, wenn sich der Kommunikationskanal beendet hat.
	 * @param thread
	 */
	public void onCommunicationWithServerClosed(ClientThread thread);
}
