package dominion.communication.tcp.server;

import java.net.Socket;

import dominion.communication.tcp.OnStringFromClient;

public class StringSenderPoolThreadServer extends PoolThreadServer {
	private OnStringFromClient string;

	public StringSenderPoolThreadServer(int port, int maxWaitTime, OnStringFromClient string) {
		super(port, maxWaitTime);
		this.string = string;
	}

	@Override
	protected StringSenderServerThread createNewServerThread(Socket client) {
		return new StringSenderServerThread(client, string);
	}
}
