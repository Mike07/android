package dominion.communication.tcp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dominion.communication.tcp.OnCommunicationOnServerClosed;


/**
 * Implementiert einen Server, der am Localhost auf Clients horcht für eine maximale, einstellbare Zeit.
 * Der Server kann darüberhinaus manuell gestoppt werden.
 * Client-Anfragen werden nacheinander blockierend abgearbeitet (alle in einem Thread).
 * Das Lauschen auf Clients geschieht in einem neuen Thread.
 * Wird der Server z.B. durch Timeout beendet, können die in neuen Threads verwendeten Verbindungen weiter
 * genutzt werden.
 * <br>
 * <br>
 * Quellen:<br>
 * - http://tutorials.jenkov.com/java-multithreaded-servers/multithreaded-server.html <br>
 * - http://tutorials.jenkov.com/java-multithreaded-servers/thread-pooled-server.html <br>
 * - Java-Insel <br>
 * - http://myandroidsolutions.blogspot.de/2012/07/android-tcp-connection-tutorial.html <br>
 * @author Johannes
 */
public abstract class PoolThreadServer implements Runnable {
	private int serverPort;
	private int maxWaitTime;
	private ServerSocket server;
	private boolean isStopped;
	private ExecutorService threadPool;

	/**
	 * Konstruktor
	 * @param port Port des Localhosts, an dem dieser Server lauschen soll
	 * @param maxWaitTime Millisekunden, die der Server maximal auf Clients warten soll (nach dem letzten Client)
	 */
	public PoolThreadServer(int port, int maxWaitTime) {
		this.serverPort = port;
		this.maxWaitTime = maxWaitTime;

		this.server = null;
		this.isStopped = false;
		this.threadPool = Executors.newCachedThreadPool();
	}

	@Override
	public void run() {
		this.openServerSocket();
		while (!this.isStopped()) {
			Socket client = null;
			try {
				client = this.server.accept();
				this.handle(client);
			} catch (SocketTimeoutException e) {
				System.out.println("Socket-Timeout!");
				this.stopServer();
			} catch (IOException e) {
				if (this.isStopped()) {
					System.out.println("Server Stopped.");
					return;
				}
				e.printStackTrace();
			} finally {
				/*
				 * client.close() darf nicht ausgeführt werden,
				 * da die Kommunikation jetzt nicht mehr blockierend ausgeführt wird,
				 * sondern in einem eigenen Thread!
				 */
			}
		}
		this.threadPool.shutdown();
		System.out.println("Server Stopped.");
	}

	public synchronized boolean isStopped() {
		return this.isStopped;
	}

	public synchronized void stopServer() {
		this.isStopped = true;
		try {
			this.server.close();
		} catch (IOException e) {
			throw new RuntimeException("Error closing server", e);
		}
	}

	private void openServerSocket() {
		try {
			this.server = new ServerSocket(this.serverPort);
			server.setSoTimeout(maxWaitTime); // maximale Wartezeit des Servers einstellen!
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port " + serverPort, e);
		}
	}

	private void handle(Socket client) {
		this.threadPool.execute(this.createNewServerThread(client));
	}

	protected abstract ServerThread createNewServerThread(Socket client);
}
