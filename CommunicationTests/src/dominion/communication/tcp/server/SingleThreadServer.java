package dominion.communication.tcp.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

import dominion.communication.tcp.OnCommunicateWithClient;

/**
 * Implementiert einen Server, der am Localhost auf Clients horcht für eine maximale, einstellbare Zeit.
 * Der Server kann darüberhinaus manuell gestoppt werden.
 * Client-Anfragen werden nacheinander blockierend abgearbeitet (alle in einem Thread).
 * Das Lauschen auf Clients geschieht in einem neuen Thread.
 * <br>
 * <br>
 * Quellen:<br>
 * - http://tutorials.jenkov.com/java-multithreaded-servers/multithreaded-server.html <br>
 * - Java-Insel <br>
 * - http://myandroidsolutions.blogspot.de/2012/07/android-tcp-connection-tutorial.html <br>
 * @author Johannes
 */
public abstract class SingleThreadServer implements Runnable, OnCommunicateWithClient {
	private int serverPort;
	private int maxWaitTime;
	private ServerSocket serverSocket = null;
	private boolean isStopped = false;

	/**
	 * Konstruktor
	 * @param port Port des Localhosts, an dem dieser Server lauschen soll
	 * @param maxWaitTime Millisekunden, die der Server maximal auf Clients warten soll (nach dem letzten Client)
	 */
	public SingleThreadServer(int port, int maxWaitTime) {
		this.serverPort = port;
		this.maxWaitTime = maxWaitTime;
	}

	@Override
	public void run() {
		this.openServerSocket();
		while (!this.isStopped()) {
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
				this.handle(clientSocket);
			} catch (SocketTimeoutException e) {
				System.out.println("Socket-Timeout!");
				this.stopServer();
			} catch (IOException e) {
				if (this.isStopped()) {
					System.out.println("Server Stopped.");
					return;
				}
				e.printStackTrace();
			} finally {
				if (clientSocket != null) {
					try {
						clientSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("Server Stopped.");
	}

	public synchronized boolean isStopped() {
		return this.isStopped;
	}

	public synchronized void stopServer() {
		this.isStopped = true;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException("Error closing server", e);
		}
	}

	private void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
			serverSocket.setSoTimeout(maxWaitTime); // maximale Wartezeit des Servers einstellen!
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port " + serverPort, e);
		}
	}

	private void handle(Socket client) {
		try {
			this.communicateWithClient(new Scanner(client.getInputStream()),
					new PrintWriter(client.getOutputStream(), true));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
