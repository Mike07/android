package dominion.communication.tcp.server;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Logger;

import dominion.communication.tcp.OnStringFromClient;

public final class StringSenderServerThread extends ServerThread {
	private boolean stopped;
	private PrintWriter out;
	private Scanner in;
	private OnStringFromClient string;

	private final static Logger logger = Logger.getLogger(StringSenderServerThread.class.getName());

	public StringSenderServerThread(Socket client, OnStringFromClient string) {
		super(client);
		this.stopped = false;
		this.string = string;
	}

	public synchronized boolean isStopped() {
		return stopped;
	}

	public synchronized void stopThread() {
		this.stopped = true;
		this.in.close();
	}

	@Override
	public void communicateWithClient(Scanner in, PrintWriter out) {
		this.out = out;
		this.in = in;
		String message = null;

		while (!this.isStopped()) {
			message = in.nextLine();
			if (message != null && string != null) {
				string.onStringFromClient(message);
			}
			message = null;
		}
	}

	public void sendStringToClient(String outString) {
//		logger.info("Server schickt Folgendes an Client: " + outString);
		this.out.println(outString);
	}
}
