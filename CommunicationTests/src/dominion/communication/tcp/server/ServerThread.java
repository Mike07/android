package dominion.communication.tcp.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

import dominion.communication.tcp.OnCommunicationOnServerClosed;
import dominion.communication.tcp.OnCommunicateWithClient;

public abstract class ServerThread implements Runnable, OnCommunicateWithClient  {
	private final Socket client;
	private OnCommunicationOnServerClosed interrupt;

	public ServerThread(Socket client) {
		super();
		this.client = client;
	}

	public final void init(OnCommunicationOnServerClosed interrupt) {		
		this.interrupt = interrupt;
	}

	protected Socket getClient() {
		return client;
	}

	@Override
	public final void run() {
		try {
			this.communicateWithClient(new Scanner(client.getInputStream()),
					new PrintWriter(client.getOutputStream(), true));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			System.err.println("Client hat die Kommunikation selbstständig beendet, " +
					"obwohl der Server noch Kommunikation erwartet hat!");
			if (interrupt != null) {
				interrupt.onCommunicationWithClientClosed(this);
			}
		}
	}
}
