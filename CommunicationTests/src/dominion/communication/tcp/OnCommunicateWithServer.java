package dominion.communication.tcp;

import java.io.PrintWriter;
import java.util.Scanner;

public interface OnCommunicateWithServer {
	public void communicateWithServer(Scanner in, PrintWriter out);
}
