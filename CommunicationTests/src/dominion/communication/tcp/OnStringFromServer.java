package dominion.communication.tcp;

public interface OnStringFromServer {
	public void onStringFromServer(String newString);
}
