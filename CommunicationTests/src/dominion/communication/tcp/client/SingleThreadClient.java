package dominion.communication.tcp.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public abstract class SingleThreadClient {
	public SingleThreadClient() {
		super();
	}

	public void init(InetAddress host, int port) {
		Socket server = null;
		try {
			server = new Socket(host, port);
			new Thread(this.createNewClientThread(server)).start();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	protected abstract ClientThread createNewClientThread(Socket server);
}
