package dominion.communication.tcp.client;

import java.net.Socket;

import dominion.communication.tcp.OnCommunicationOnClientClosed;
import dominion.communication.tcp.OnStringFromServer;

public class StringSenderSingleThreadClient extends SingleThreadClient {
	private OnStringFromServer string;
	private StringSenderClientThread thread;

	public StringSenderSingleThreadClient(OnStringFromServer string) {
		super();
		this.string = string;
	}

	public final void init(OnCommunicationOnClientClosed interrupt) {		
		thread.init(interrupt);
	}

	@Override
	protected ClientThread createNewClientThread(Socket server) {
		thread = new StringSenderClientThread(server, string);
		return thread;
	}

	public void sendStringToServer(String outString) {
		thread.sendStringToServer(outString);
	}

	public void stop() {
		thread.stopThread();
	}
}
