package dominion.communication.tcp.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

import dominion.communication.tcp.OnCommunicateWithServer;
import dominion.communication.tcp.OnCommunicationOnClientClosed;

public abstract class ClientThread implements Runnable, OnCommunicateWithServer {
	private Socket server;
	private OnCommunicationOnClientClosed interrupt;

	public ClientThread(Socket server) {
		super();
		this.server = server;
	}

	public final void init(OnCommunicationOnClientClosed interrupt) {		
		this.interrupt = interrupt;
	}

	@Override
	public void run() {
		try {
			this.communicateWithServer(new Scanner(server.getInputStream()),
						new PrintWriter(server.getOutputStream(), true));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			System.err.println("Server hat die Kommunikation selbstständig beendet, " +
					"obwohl der Client noch Kommunikation erwartet hat!");
			if (interrupt != null) {
				interrupt.onCommunicationWithServerClosed(this);
			}
		}
	}
}
