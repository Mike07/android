package dominion.communication.tcp.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

import dominion.communication.tcp.OnCommunicateWithServer;



public abstract class BlockedClient implements OnCommunicateWithServer {
	public BlockedClient(InetAddress host, int port) {
		super();
		Socket server = null;
		try {
			server = new Socket(host, port);
			Scanner in = new Scanner(server.getInputStream());
			PrintWriter out = new PrintWriter(server.getOutputStream(), true);

			this.communicateWithServer(in, out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
