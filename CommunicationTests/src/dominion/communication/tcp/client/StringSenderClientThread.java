package dominion.communication.tcp.client;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Logger;

import dominion.communication.tcp.OnStringFromServer;

public class StringSenderClientThread extends ClientThread {
	private boolean stopped;
	private PrintWriter out;
	private Scanner in;
	private OnStringFromServer string;

	private final static Logger logger = Logger.getLogger(StringSenderClientThread.class.getName());

	public StringSenderClientThread(Socket server, OnStringFromServer string) {
		super(server);
		this.stopped = false;
		this.string = string;
		if (this.string == null) {
			throw new IllegalArgumentException("ist null");
		}
	}

	public synchronized boolean isStopped() {
		return stopped;
	}

	public synchronized void stopThread() {
		this.stopped = true;
		in.close();
	}

	@Override
	public void communicateWithServer(Scanner in, PrintWriter out) {
		this.out = out;
		this.in = in;
		String message = null;

		while (!this.isStopped()) {
			message = in.nextLine();
			if (message != null && string != null) {
				string.onStringFromServer(message);
			}
			message = null;
		}
	}

	public void sendStringToServer(String outString) {
//		logger.info("Client schickt Folgendes an Server: " + outString);
		this.out.println(outString);
	}
}
