package dominion.communication.tcp;

import dominion.communication.tcp.server.ServerThread;

public interface OnCommunicationOnServerClosed {
	/**
	 * Wird aufgerufen, wenn sich der Kommunikationskanal beendet hat.
	 * @param thread
	 */
	public void onCommunicationWithClientClosed(ServerThread thread);
}
