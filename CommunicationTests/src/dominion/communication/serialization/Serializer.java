package dominion.communication.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.logging.Logger;

import biz.source_code.base64Coder.Base64Coder;

/**
 * Serialisiert und Deserialisiert beliebige Serializable-Objekte in/aus einen String.<br>
 * Verwendete Quellen:<br>
 * - http://www.source-code.biz/base64coder/java/ <br>
 * - Java-Insel <br>
 * - http://stackoverflow.com/questions/134492/how-to-serialize-an-object-into-a-string <br>
 * @author Johannes
 */
public abstract class Serializer {
	private final static Logger logger = Logger.getLogger(Serializer.class.getName());

	public static String serializeToString(Serializable object) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String r = null;
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			oos.close();
			// sinnvolle Codierung per JAR von http://www.source-code.biz/base64coder/java/
			r = new String(Base64Coder.encode(baos.toByteArray()));
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		logger.info("serialize: " + r);
		return r;
	}

	public static Object deserializeToObject(String object) throws CommunicationSerializationException {
		try {
			// sinnvolle Codierung per JAR von http://www.source-code.biz/base64coder/java/
			byte[] data = Base64Coder.decode(object);
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
			Object o = ois.readObject();
			ois.close();
			return o;		
		} catch (IOException e) {
			logger.info("Deserialisierungs-Eingabe: " + object);
			e.printStackTrace();
			throw new CommunicationSerializationException(e);
		} catch (ClassNotFoundException e) {
			logger.info("Deserialisierungs-Eingabe: " + object);
			e.printStackTrace();
			throw new CommunicationSerializationException(e);
		} catch (Throwable e) {
			logger.info("Deserialisierungs-Eingabe: " + object);
			e.printStackTrace();			
			throw new CommunicationSerializationException(e);
		}
	}
}
