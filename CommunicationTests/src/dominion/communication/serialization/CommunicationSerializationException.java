package dominion.communication.serialization;

/**
 * Exception, die geworfen wird, wenn die Deserialisierung fehlgeschlagen ist
 * @author Johannes
 */
@SuppressWarnings("serial")
public class CommunicationSerializationException extends Exception {
	public CommunicationSerializationException(Throwable error) {
		super(error);
	}
}
