package serialization;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SomeTestClass implements Serializable {
	private int i = Integer.MAX_VALUE;
	private String s = "ABCDEFGHIJKLMNOP";
	private Double d = new Double(-1.0);

	@Override
	public String toString() {
		return "SomeClass instance says: Don't worry, "
				+ "I'm healty. Look, my data is i = " + i + ", s = " + s
				+ ", d = " + d;
	}
}
