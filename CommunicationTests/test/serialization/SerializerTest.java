package serialization;

import dominion.communication.serialization.Serializer;

/**
 * Test des Serializierers
 * @author Johannes
 */
public class SerializerTest {
	public static void main(String[] args) {
		SomeTestClass some = new SomeTestClass();
		System.out.println(some);
		String s = Serializer.serializeToString(some);
		System.out.println(s);
		SomeTestClass r = (SomeTestClass) Serializer.deserializeToObject(s);
		System.out.println(r);
	}
}
