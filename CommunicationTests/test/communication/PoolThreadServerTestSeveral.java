package communication;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.client.BlockedClient;
import dominion.communication.tcp.server.PoolThreadServer;
import dominion.communication.tcp.server.ServerThread;
import dominion.communication.util.ThreadUtilities;



public class PoolThreadServerTestSeveral extends BlockedClient {
	private static PoolThreadServer serv;

	public PoolThreadServerTestSeveral(InetAddress host, int port) {
		super(host, port);
	}

	public static void main(String[] args) {
		startServer();

		try {
			new PoolThreadServerTestSeveral(InetAddress.getByName("localhost"), 12345);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		warte(3000, true);
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new PoolThreadServer(12345, 5000) {
			@Override
			protected ServerThread createNewServerThread(Socket client) {
				return new ServerThread(client) {
					@Override
					public void communicateWithClient(Scanner in, PrintWriter out) {
						int i = 0;
						while (i < 8) {
							out.println("hallo: " + i);
							warte(1000, false);
							i++;
						}
					}
				};
			}
		};
		new Thread(serv).start();		

		warte(2000, true);
	}

	private static void warte(int millis, boolean client) {
		ThreadUtilities.sleep(millis);
	}

	@Override
	public void communicateWithServer(Scanner in, PrintWriter out) {
		int i = 0;
		while (i < 8) {
			System.out.println("1: " + in.nextLine());
			i++;
		}
	}
}
