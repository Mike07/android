package communication;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.client.BlockedClient;



public class SecondClient extends BlockedClient {
	public SecondClient(InetAddress host, int port) {
		super(host, port);
	}

	@Override
	public void communicateWithServer(Scanner in, PrintWriter out) {
		int i = 0;
		while (i < 6) {
			System.out.println("2: " + in.nextLine());
			i++;
		}
//		out.println("2");
//		out.println("4");
//		System.out.println("Client: 2 gesendet! jetzt: warten");
//		System.out.println("Client: 2 Ergebnis da: " + in.nextLine());
	}

	public static void main(String[] args) {
		try {
			new SecondClient(InetAddress.getByName("localhost"), 12345);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
