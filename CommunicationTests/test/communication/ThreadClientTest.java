package communication;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.client.ClientThread;
import dominion.communication.tcp.client.SingleThreadClient;
import dominion.communication.tcp.server.SingleThreadServer;
import dominion.communication.util.ThreadUtilities;



public class ThreadClientTest extends SingleThreadClient {
	private static SingleThreadServer serv;

	public ThreadClientTest() {
		super();
	}

	public static void main(String[] args) {
		startServer();

		try {
			ThreadClientTest test = new ThreadClientTest();
			test.init(InetAddress.getByName("localhost"), 12345);
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new SingleThreadServer(12345, 8000) {
			@Override
			public void communicateWithClient(Scanner in, PrintWriter out) {
				String factor1 = in.nextLine();
				String factor2 = in.nextLine();

				warte(3000);

				out.println(new BigInteger(factor1).multiply(new BigInteger(factor2)));
			}			
		};
		new Thread(serv).start();		

		warte(2000);
	}

	private static void warte(int millis) {
		System.out.println("warte " + millis + " Millisekunden");
		ThreadUtilities.sleep(millis);
	}

	@Override
	protected ClientThread createNewClientThread(Socket server) {
		return new ClientThread(server) {
			@Override
			public void communicateWithServer(Scanner in, PrintWriter out) {
				out.println("2");
				out.println("4");
				System.out.println("1 gesendet! jetzt: warten");
				System.out.println("1 Ergebnis da: " + in.nextLine());
			}
		};
	}
}
