package communication;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.server.SingleThreadServer;
import dominion.communication.util.ThreadUtilities;



public class SingleThreadServerTest extends SingleThreadServer {
	private static SingleThreadServer serv;

	public SingleThreadServerTest(int port, int maxWaitTime) {
		super(port, maxWaitTime);
	}

	public static void main(String[] args) {
		startServer();

		Socket server = null;
		try {
			server = new Socket("localhost", 12345);
			Scanner in = new Scanner(server.getInputStream());
			PrintWriter out = new PrintWriter(server.getOutputStream(), true);

			out.println("2");
			out.println("4");
			System.out.println("1 gesendet! jetzt: warten");
			System.out.println("1 Ergebnis da: " + in.nextLine());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		warte(2000);

		serv.stopServer();
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new SingleThreadServerTest(12345, 5000);
		new Thread(serv).start();		

		warte(2000);
	}

	private static void warte(int millis) {
		System.out.println("warte " + millis + " Millisekunden");
		ThreadUtilities.sleep(millis);
	}

	@Override
	public void communicateWithClient(Scanner in, PrintWriter out) {
		String factor1 = in.nextLine();
		String factor2 = in.nextLine();

		ThreadUtilities.sleep(3000);

		out.println(new BigInteger(factor1).multiply(new BigInteger(factor2)));
	}
}
