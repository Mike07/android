package communication;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.client.BlockedClient;
import dominion.communication.tcp.server.PoolThreadServer;
import dominion.communication.tcp.server.ServerThread;
import dominion.communication.util.ThreadUtilities;



public class PoolThreadServerTest extends BlockedClient {
	private static PoolThreadServer serv;

	public PoolThreadServerTest(InetAddress host, int port) {
		super(host, port);
	}

	public static void main(String[] args) {
		startServer();

		try {
			new PoolThreadServerTest(InetAddress.getByName("localhost"), 12345);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		warte(3000, true);
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new PoolThreadServer(12345, 10000) {
			@Override
			protected ServerThread createNewServerThread(Socket client) {
				return new ServerThread(client) {
					@Override
					public void communicateWithClient(Scanner in, PrintWriter out) {
						int i = 0;
//						while (true) {
						/*
						 * i < 2: Client wartet ewig (?), (Server meldet sich insgesamt ab => ist aber egal!), TODO
						 * i < 3: genau richtig eingestellt (d.h. genau entsprechend der Client-Kommunikation-Implementierung
						 * i < 4: Server erwartet Eingabe, aber Client schließt sich selbst => NoSuchElementException => wird jetzt abgefangen
						 */
						while (i < 3) {
							String factor1 = in.nextLine();
							String factor2 = in.nextLine();

							warte(2000, false);

							out.println(new BigInteger(factor1).multiply(new BigInteger(factor2)));

							i++;
						}
					}
				};
			}
		};
		new Thread(serv).start();		

		warte(2000, true);
	}

	private static void warte(int millis, boolean client) {
		if (client) {
			System.out.println("Client: 1 warte " + millis + " Millisekunden");
		} else {
			System.out.println("Server: 1 warte " + millis + " Millisekunden");			
		}
		ThreadUtilities.sleep(millis);
	}

	@Override
	public void communicateWithServer(Scanner in, PrintWriter out) {
		this.mult(in, out, 2, 4);
		warte(1000, true);
		this.mult(in, out, 3, 5);
		warte(1000, true);
		this.mult(in, out, 4, 6);
	}

	private void mult(Scanner in, PrintWriter out, int eins, int zwei) {
		out.println(eins);
		out.println(zwei);
		System.out.println("Client: 1 gesendet! jetzt: warten");
		System.out.println("Client: 1 Ergebnis da: " + in.nextLine());		
	}
}
