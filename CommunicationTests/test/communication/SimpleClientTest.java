package communication;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.client.BlockedClient;
import dominion.communication.tcp.server.SingleThreadServer;
import dominion.communication.util.ThreadUtilities;



public class SimpleClientTest extends BlockedClient {
	private static SingleThreadServer serv;

	public SimpleClientTest(InetAddress host, int port) {
		super(host, port);
	}

	@Override
	public void communicateWithServer(Scanner in, PrintWriter out) {
		out.println("2");
		out.println("4");
		System.out.println("1 gesendet! jetzt: warten");
		System.out.println("1 Ergebnis da: " + in.nextLine());
	}

	public static void main(String[] args) {
		startServer();

		try {
			new SimpleClientTest(InetAddress.getByName("localhost"), 12345);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		warte(2000);

		serv.stopServer();
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new SingleThreadServer(12345, 5000) {
			@Override
			public void communicateWithClient(Scanner in, PrintWriter out) {
				String factor1 = in.nextLine();
				String factor2 = in.nextLine();

				warte(3000);

				out.println(new BigInteger(factor1).multiply(new BigInteger(factor2)));
			}			
		};
		new Thread(serv).start();		

		warte(2000);
	}

	private static void warte(int millis) {
		System.out.println("warte " + millis + " Millisekunden");
		ThreadUtilities.sleep(millis);
	}
}
