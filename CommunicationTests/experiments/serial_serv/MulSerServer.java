package serial_serv;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

import dominion.communication.serialization.Serializer;


/**
 * verwendete Quellen:
 * - http://myandroidsolutions.blogspot.de/2012/07/android-tcp-connection-tutorial.html
 * - Java-Insel
 * @author Johannes
 */
/*
 * Erkenntnisse:
 * - (De-)Serialisierung funktioniert auch problemlos, wenn die Strings über Sockets verschickt werden.
 */
public class MulSerServer {
	public static void main(String[] args) {
		ServerSocket server = null;
		try {
			server = new ServerSocket(12345);
			server.setSoTimeout(8000); // maximale Wartezeit des Servers einstellen!
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (server != null && !server.isClosed()) { // oder einfach nur "true"??
			Socket client = null;
			try {
				client = server.accept(); // Server wartet blockierend auf Client-Anfrage
				handle(client); // Protokoll mit Client durchführen
			} catch (SocketTimeoutException e) {
				// max. Wartezeit nach dem letzten "accept()" ist abgelaufen!
				System.out.println("Socket-Timeout!");
				try {
					if (client != null) {
						client.close();
					}
					/*
					 * Server muss geschlossen werden, da die Exception nichts selbst tut.
					 * Wird der Server nicht geschlossen, wird nur die Exception im angegebenen Intervall
					 * immer wieder geworfen ohne sonstige Auswirkungen!
					 */
					server.close();
				} catch (IOException ie) {
					ie.printStackTrace();
				}
			} catch (InterruptedIOException e) {
				System.err.println("Server wurde abgemeldet!");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (client != null) {
					try {
						client.close(); // Client schließen (schließt dabei auch In- und OutputStreams!)
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
//			System.out.println("Client-Kommunikation abgeschlossen: jetzt nächster Client!");
		}
		System.out.println("Programm ist fertig!");
	}

	private static void handle(Socket client) throws IOException {
		Scanner in = new Scanner(client.getInputStream());
		PrintWriter out = new PrintWriter(client.getOutputStream(), true);

		String string = in.nextLine();

		Serializable o = (Serializable) Serializer.deserializeToObject(string);
		string = Serializer.serializeToString(o);

		out.println(string);
	}
}
