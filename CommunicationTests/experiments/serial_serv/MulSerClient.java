package serial_serv;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.serialization.Serializer;

import serialization.SomeTestClass;

/**
 * verwendete Quellen:
 * - http://myandroidsolutions.blogspot.de/2012/07/android-tcp-connection-tutorial.html
 * - Java-Insel
 * @author Johannes
 */
public class MulSerClient {
	public static void main(String[] args) {
		Socket server = null;
		try {
			server = new Socket("localhost", 12345);
			Scanner in = new Scanner(server.getInputStream());
			PrintWriter out = new PrintWriter(server.getOutputStream(), true);

			SomeTestClass c = new SomeTestClass();
			System.out.println(c);
			String sc = Serializer.serializeToString(c);

			out.println(sc);
			String res = in.nextLine();

			System.out.println(res);
			System.out.println(Serializer.deserializeToObject(res));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
