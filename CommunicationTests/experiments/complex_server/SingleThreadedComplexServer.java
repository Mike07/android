package complex_server;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

import dominion.communication.util.ThreadUtilities;


/**
 * Quellen:
 * - http://tutorials.jenkov.com/java-multithreaded-servers/multithreaded-server.html
 * @author Johannes
 */
public class SingleThreadedComplexServer implements Runnable {
	private int serverPort;
	private int maxWaitTime;
	protected ServerSocket serverSocket = null;
	private boolean isStopped = false;

	public SingleThreadedComplexServer(int port, int maxWaitTime) {
		this.serverPort = port;
		this.maxWaitTime = maxWaitTime;
	}

	public void run() {
		this.openServerSocket();
		while (!this.isStopped()) {
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
				this.handle(clientSocket);
			} catch (SocketTimeoutException e) {
				System.out.println("Socket-Timeout!");
				this.stopServer();
			} catch (IOException e) {
				if (this.isStopped()) {
					System.out.println("Server Stopped.");
					return;
				}
				e.printStackTrace();
			} finally {
				if (clientSocket != null) {
					try {
						clientSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("Server Stopped.");
	}

	private synchronized boolean isStopped() {
		return this.isStopped;
	}

	public synchronized void stopServer() {
		this.isStopped = true;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException("Error closing server", e);
		}
	}

	private void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
			serverSocket.setSoTimeout(maxWaitTime); // maximale Wartezeit des Servers einstellen!
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port " + serverPort, e);
		}
	}

	private void handle(Socket client) {
		try {
			this.handleClient(new Scanner(client.getInputStream()), new PrintWriter(client.getOutputStream(), true));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void handleClient(Scanner in, PrintWriter out) {
		String factor1 = in.nextLine();
		String factor2 = in.nextLine();

		ThreadUtilities.sleep(4000);

		out.println(new BigInteger(factor1).multiply(new BigInteger(factor2)));
	}
}
