package complex_server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.util.ThreadUtilities;


/**
 * verwendete Quellen:
 * - http://myandroidsolutions.blogspot.de/2012/07/android-tcp-connection-tutorial.html
 * - Java-Insel
 * @author Johannes
 */
public class MulBlockedClient {
	private static SingleThreadedComplexServer serv;

	public static void main(String[] args) {
		startServer();

		Socket server = null;
		try {
			server = new Socket("localhost", 12345);
			Scanner in = new Scanner(server.getInputStream());
			PrintWriter out = new PrintWriter(server.getOutputStream(), true);

			out.println("2");
			out.println("4");
			System.out.println("1 gesendet! jetzt: warten");
			System.out.println("1 Ergebnis da: " + in.nextLine());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		int w = 2000;
		System.out.println("warte " + w + " Millisekunden bis zum Server-Stop");
		ThreadUtilities.sleep(w);

		serv.stopServer();
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new SingleThreadedComplexServer(12345, 5000);
		new Thread(serv).start();		

		int w = 2000;
		System.out.println("... und warte " + w + " Millisekunden");

		ThreadUtilities.sleep(w);
	}
}
