package closing;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * verwendete Quellen:
 * - Java-Insel
 * @author Johannes
 */
/*
 * Erkenntnisse:
 * - Eine client-seitige Blockade (z.B. weil der Server nicht antwortet) kann durch das Schließen des Client-Sockets
 * aufgehoben werden.
 */
public class CloseConnection {
	public static void main(String[] args) throws Exception {
		ServerSocket server = new ServerSocket(12345);
		final Socket t = new Socket("localhost", 12345);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					System.out.println("hängt gleich!");
					System.out.println(t.getInputStream().read());
					System.out.println("jetzt hängt er!");
				} catch (IOException e) {
					System.out.println("Blockierung aufgehoben!");
				}
			}
		}).start();
		Thread.sleep(2000);
		t.close(); // das Schließen der Verbindung hebt das blockierende Warten auf den Server auf!
		server.close();
	}
}
