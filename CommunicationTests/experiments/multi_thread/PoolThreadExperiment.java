package multi_thread;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import dominion.communication.tcp.client.BlockedClient;
import dominion.communication.tcp.server.PoolThreadServer;
import dominion.communication.tcp.server.ServerThread;
import dominion.communication.util.ThreadUtilities;



/**
 * Testet erfolgreich, dass der Server-Thread sehr lange Daten an den Client schicken kann.
 * @author Johannes
 */
public class PoolThreadExperiment extends BlockedClient {
	private static PoolThreadServer serv;

	public PoolThreadExperiment(InetAddress host, int port) {
		super(host, port);
	}

	public static void main(String[] args) {
		startServer();

		try {
			new PoolThreadExperiment(InetAddress.getByName("localhost"), 12345);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		warte(2000);
	}

	private static void startServer() {
		System.out.println("starte Server ...");
		serv = new PoolThreadServer(12345, 10000) {
			@Override
			protected ServerThread createNewServerThread(Socket client) {
				return new ServerThread(client) {
					@Override
					public void communicateWithClient(Scanner in, PrintWriter out) {
						int i = 0;
						while (true) {
//						while (i < 4) {
							warte(500);
							out.println("hallo: " + i);

							i++;
						}
					}
				};
			}
		};
		new Thread(serv).start();		

		warte(2000);
	}

	private static void warte(int millis) {
//		System.out.println("warte " + millis + " Millisekunden");
		ThreadUtilities.sleep(millis);
	}

	@Override
	public void communicateWithServer(Scanner in, PrintWriter out) {
		int i = 0;
		while (true) {
//		while (i < 6) {
			System.out.println(in.nextLine());
		}
	}
}
