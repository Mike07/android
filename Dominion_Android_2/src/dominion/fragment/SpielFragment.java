package dominion.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.SpielModell;

public class SpielFragment extends Fragment {
	private SpielModell spiel;

	public SpielFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_spiel, container, false);
		return view;
	}

	public void setSpiel(SpielModell spiel) {
		this.spiel = spiel;
		// TODO: spielabhängig gestalten!
	}
}
