package dominion.fragment;

import dominion.ActivityNavigationManager;
import dominion.DominionApplication;
import dominion.activity.LobbyActivity;
import dominion.adapter.LobbyExpandableListAdapter;
import dominion.adapter.LobbyListAdapter;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.elements.SpielerModell;
import dominion.miniModell.modell.observer.addDelete.SpielAddDeleteObserver;
import dominion.miniModell.modell.observer.addDelete.SpielerAddDeleteObserver;
import dominion.miniModell.modell.observer.update.SpielObserver;
import dominion.miniModell.modell.types.SpielStatus;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;

public class LobbyInputFragment extends Fragment implements SpielAddDeleteObserver, SpielObserver, SpielerAddDeleteObserver {
	private ExpandableListView portraitInputView;
	private ListView landscapeInputView;
	private LobbyDetailsFragment ldf;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_lobby_input, container, false);
		if (view instanceof ExpandableListView) {
			this.portraitInputView = (ExpandableListView) view;
			this.landscapeInputView = null;
		} else if (view instanceof ListView) {
			this.portraitInputView = null;
			this.landscapeInputView = (ListView) view;
		}

		return view;
	}

	public void setAdaptersAndData(Context context) {
		final LobbyActivity activity = (LobbyActivity) context;
		int selection = activity.getSelection();
		if (portraitInputView != null) {
			LobbyExpandableListAdapter adapter = new LobbyExpandableListAdapter(context);
			portraitInputView.setAdapter(adapter);
			portraitInputView.setOnGroupClickListener(new OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					if (parent.expandGroup(groupPosition)) {
						activity.setSelection(groupPosition);
					} else {
						parent.collapseGroup(groupPosition);
						activity.setSelection(-1);
					}
					return true;
				}
			});
			if (selection > -1 && selection < adapter.getGroupCount()) {
				portraitInputView.expandGroup(selection);
			}
		} else if (landscapeInputView != null) {
			final LobbyListAdapter adapter = new LobbyListAdapter(context);
			landscapeInputView.setAdapter(adapter);
			landscapeInputView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					LobbyDetailsFragment detailsFragment = (LobbyDetailsFragment)
							getFragmentManager().findFragmentById(R.id.lobby_details_fragment);
					detailsFragment.setData(position, adapter.getGames().get(position));
					ldf = detailsFragment;
					activity.setSelection(position);
				}
			});
			if (selection > -1 && selection < adapter.getCount()) {
				LobbyDetailsFragment detailsFragment = (LobbyDetailsFragment)
						getFragmentManager().findFragmentById(R.id.lobby_details_fragment);
				detailsFragment.setData(selection, adapter.getGames().get(selection));
			}
		}
		DominionApplication.getInstance().getManager().getModell().addSpielAddDeleteObserver(this, true);
	}

	private void notifyDataSetChanged() {
		new AsyncTask<Void, Void, Void> () {
			@Override
			protected Void doInBackground(Void... params) {
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				if (portraitInputView != null) {
					((LobbyExpandableListAdapter) portraitInputView.getExpandableListAdapter()).notifyDataSetChanged();
				} else if (landscapeInputView != null) {
					LobbyListAdapter adapter = (LobbyListAdapter) landscapeInputView.getAdapter();
					adapter.notifyDataSetChanged();
					int selection = ldf.getPosition();
					if (selection > -1 && selection < adapter.getCount()) {
						ldf.setData(selection, adapter.getGames().get(selection));
					}
				}
				super.onPostExecute(result);
			}
		}.execute();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void addSpiel(SpielModell spiel) {
		new AsyncTask<SpielModell, Void, SpielModell>() {
			@Override
			protected SpielModell doInBackground(SpielModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(SpielModell result) {
				if (result.getSpielStatus() != SpielStatus.SPIEL_BEENDET) {
					if (portraitInputView != null) {
						LobbyExpandableListAdapter adapter = (LobbyExpandableListAdapter) portraitInputView.getExpandableListAdapter();
						adapter.getGames().add(result);
						adapter.notifyDataSetChanged();
					} else if (landscapeInputView != null) {
						LobbyListAdapter adapter = (LobbyListAdapter) landscapeInputView.getAdapter();
						adapter.getGames().add(result);
						adapter.notifyDataSetChanged();
					}
					result.addSpielObserver(LobbyInputFragment.this, false);
					result.addSpielerAddDeleteObserver(LobbyInputFragment.this, false);

					if (DominionApplication.getInstance().getActionBarSelection() != ActivityNavigationManager.LOBBY) {
						DominionApplication.getInstance().issueNotification(
								getString(R.string.notify_new_game_title),
								result.getName(),
								LobbyActivity.class);
					}
				}
			}
		}.execute(spiel);
	}

	@Override
	public void deleteSpiel(SpielModell spiel) {
		new AsyncTask<SpielModell, Void, SpielModell>() {
			@Override
			protected SpielModell doInBackground(SpielModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(SpielModell result) {
				result.removeSpielObserver(LobbyInputFragment.this);
				result.removeSpielerAddDeleteObserver(LobbyInputFragment.this);
				if (portraitInputView != null) {
					LobbyExpandableListAdapter adapter = (LobbyExpandableListAdapter) portraitInputView.getExpandableListAdapter();
					adapter.getGames().remove(result);
					adapter.notifyDataSetChanged();
				} else if (landscapeInputView != null) {
					LobbyListAdapter adapter = (LobbyListAdapter) landscapeInputView.getAdapter();
					adapter.getGames().remove(result);
					adapter.notifyDataSetChanged();
				}
			}
		}.execute(spiel);
	}

	@Override
	public void updateAll(SpielModell spiel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAktuelleKartenTypen(SpielModell spiel) {
		notifyDataSetChanged();
	}

	@Override
	public void updateAddBeobachter(SpielModell spiel, NutzerModell beobachter) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDeleteBeobachter(SpielModell spiel, NutzerModell beobachter) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAktuellerSpieler(SpielModell spiel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePhaseStatus(SpielModell spiel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateSpielStatus(SpielModell spiel) {
		new AsyncTask<SpielModell, Void, SpielModell>() {
			@Override
			protected SpielModell doInBackground(SpielModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(SpielModell result) {
				if (result.getSpielStatus() == SpielStatus.SPIEL_BEENDET) {
					result.removeSpielObserver(LobbyInputFragment.this);
					result.removeSpielerAddDeleteObserver(LobbyInputFragment.this);
					if (portraitInputView != null) {
						LobbyExpandableListAdapter adapter = (LobbyExpandableListAdapter) portraitInputView.getExpandableListAdapter();
						adapter.getGames().remove(result);
						adapter.notifyDataSetChanged();
					} else if (landscapeInputView != null) {
						LobbyListAdapter adapter = (LobbyListAdapter) landscapeInputView.getAdapter();
						adapter.getGames().remove(result);
						adapter.notifyDataSetChanged();
					}
				}
			}
		}.execute(spiel);
	}

	@Override
	public void addSpieler(SpielerModell spieler) {
		notifyDataSetChanged();
	}

	@Override
	public void deleteSpieler(SpielerModell spieler) {
		notifyDataSetChanged();
	}
}
