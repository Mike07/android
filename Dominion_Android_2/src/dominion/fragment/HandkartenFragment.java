package dominion.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.ui.KarteAnimation;
import dominion.ui.KartenAuslageView;

public class HandkartenFragment extends Fragment {
	private KarteAnimation animation;

	public HandkartenFragment() {
		// Required empty public constructor
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			animation = (KarteAnimation) activity;
			if (animation == null) {
				throw new IllegalStateException("Activity ist null?!");
			}
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement KarteAnimation");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_handkarten, container, false);

		PictureLoader kl = PictureLoader.getInstance();
		ArrayList<KarteModell> karten = new ArrayList<KarteModell>();

		karten.add(kl.createDorf());
//		karten.get(0).setChecked(true);
		karten.add(kl.createDorf());
//		karten.get(1).setAufgedeckt(false);
		karten.add(kl.createRatsversammlung());
//		karten.get(2).setCountNumbers(true);
//		karten.get(2).setCountNumber(2);
		karten.add(kl.createDorf());
//		karten.get(3).setCountNumbers(true);
//		karten.get(3).setCountNumber(10);
//		karten.get(3).setChecked(true);
		karten.add(kl.createRatsversammlung());
//		karten.get(4).setCheckedNumber(2);
		karten.add(kl.createDorf());
//		karten.get(5).setCheckedNumber(10);
//		karten.get(5).setAktiv(false);
		karten.add(kl.createBibliothek());
//		karten.get(6).setCheckedNumber(11);
		karten.add(kl.createDorf());
		karten.add(kl.createRatsversammlung());
		karten.add(kl.createDorf());
		karten.add(kl.createRatsversammlung());
		karten.add(kl.createDorf());

		karten.add(kl.createBibliothek());
//		karten.get(12).setChecked(true);

		KartenAuslageView list = (KartenAuslageView) view.findViewById(R.id.spiel_fragment_hlist);
		list.setOwnHandler(null);
		list.setAnimationKarte(animation);
		list.setKarten(karten);

		return view;
	}
}
