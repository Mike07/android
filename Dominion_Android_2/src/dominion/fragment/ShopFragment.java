package dominion.fragment;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.SpielModell;

public class ShopFragment extends Fragment {
	private SpielModell spiel;
	private ShopListFragment firstList;
	private ShopListFragment secondList;

	public ShopFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_shop, container, false);

		firstList = (ShopListFragment) getFragmentManager().findFragmentById(R.id.shop_fragment_list_fragment);
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			secondList = (ShopListFragment) getFragmentManager().findFragmentById(R.id.shop_fragment_list_fragment_zwei);			
		} else {
			secondList = null;
		}

		return view;
	}

	public void setSpiel(SpielModell spiel) {
		this.spiel = spiel;
		if (secondList == null) {
			firstList.setSpiel(this.spiel, false, false);
		} else {
			firstList.setSpiel(this.spiel, true, false);			
			secondList.setSpiel(this.spiel, false, true);			
		}
	}
}
