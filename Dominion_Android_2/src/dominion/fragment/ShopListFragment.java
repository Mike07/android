package dominion.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import dominion.adapter.SimpleShopAdapter;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KartenStapelModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.ui.KarteAnimation;

public class ShopListFragment extends Fragment {
	private SpielModell spiel;
	private ListView list;
	private KarteAnimation animation;

	public ShopListFragment() {
		// Required empty public constructor
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			animation = (KarteAnimation) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement KarteAnimation and OnTouchedKartenAuslageView!");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_shop_list, container, false);
		list = (ListView) view.findViewById(R.id.shop_list_fragment_list);
		return view;
	}

	public void setSpiel(SpielModell spiel, boolean nurKoenigreich, boolean nurBasis) {
		this.spiel = spiel;
		List<KartenStapelModell> stapel = new ArrayList<KartenStapelModell>();
		// TODO proper error handling
		if (spiel != null) {
			if (nurKoenigreich) {
				for (KartenStapelModell s : this.spiel.getVorrat()) {
					if (s.getPlatzhalterKartenTyp().isIstKoenigreichkarte()) {
						stapel.add(s);
					}
				}
			} else if (nurBasis) {
				for (KartenStapelModell s : this.spiel.getVorrat()) {
					if (! s.getPlatzhalterKartenTyp().isIstKoenigreichkarte()) {
						stapel.add(s);
					}
				}
			} else {
				stapel = this.spiel.getVorrat();
			}
		} else {
			System.err.println("The server appears to be down.");
		}
		SimpleShopAdapter adapter = new SimpleShopAdapter(getActivity(), stapel, animation);
		this.list.setAdapter(adapter);
	}
}
