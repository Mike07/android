package dominion.fragment;

import java.util.ArrayList;
import java.util.List;

import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KartenTypModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.elements.SpielerModell;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LobbyDetailsFragment extends Fragment {

	private int position;
	private View detailsView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.detailsView = inflater.inflate(R.layout.fragment_lobby_details, container, false);
		return detailsView;
	}
	
	public void setData(int position, SpielModell game) {
		this.position = position;
		TextView cardsView = (TextView) detailsView.findViewById(R.id.lobby_details_cards_list);
		game.getAktuelleKartenTypen();
		cardsView.setText(cardsToMultiLineText(game.getAktuelleKartenTypen()));
		TextView playersView = (TextView) detailsView.findViewById(R.id.lobby_details_players_list);
		playersView.setText(playersToMultiLineText(game.getSpieler()));
	}
	
	public static String cardsToMultiLineText(List<KartenTypModell> types) {
		List<String> typeNames = new ArrayList<String>();
		for (KartenTypModell type : types) {
			typeNames.add(type.getName());
		}
		return toMultiLineText(typeNames);
	}
	
	public static String playersToMultiLineText(List<SpielerModell> players) {
		List<String> userNames = new ArrayList<String>();
		for (SpielerModell player : players) {
			if (player != null && player.getNutzer() != null && player.getNutzer().getNickname() != null) {
				userNames.add(player.getNutzer().getNickname());
			} else {
				userNames.add("<null>");
			}
		}
		return toMultiLineText(userNames);
	}
	
	public static String toMultiLineText(List<String> lines) {
		if (lines == null || lines.size() == 0)
			return new String("");
		
		StringBuilder multiLine = new StringBuilder();
		multiLine.append(lines.get(0));
		for (int i = 1; i < lines.size(); i++) {
			multiLine.append(String.format("%n%s", lines.get(i)));
		}
		
		return multiLine.toString();
	}
	
	public int getPosition() {
		return this.position;
	}

}
