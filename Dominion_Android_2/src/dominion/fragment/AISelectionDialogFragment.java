package dominion.fragment;

import java.util.ArrayList;
import java.util.List;

import dominion.DominionApplication;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KiModell;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class AISelectionDialogFragment extends DialogFragment {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.select_ai);
		List<KiModell> ais = DominionApplication.getInstance().getManager().getModell().getKis();
		List<String> aiNames = new ArrayList<String>();
		for (KiModell ai : ais) {
			aiNames.add(ai.getName());
		}
		builder.setSingleChoiceItems(new ArrayAdapter<String>(getActivity(), R.layout.general_purpose_text_large, aiNames), 0, null);
		builder.setPositiveButton(R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		builder.setNegativeButton(R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		return builder.create();
	}

}
