package dominion.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import dominion.DominionApplication;
import dominion.adapter.SpielerBildAdapter;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.ui.HorizontalListView;

public class InfoFragment extends Fragment {
	private SpielModell spiel;
	private SpielerBildAdapter adapter;

	public InfoFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_info, container, false);

		RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.info_fragment_layout);

		AdapterView<ListAdapter> l; // <ListAdapter> 
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//			ListView lv = (ListView) view.findViewById(R.id.info_fragment_spieler_liste);
//			l = lv;
			ListView lv = new ListView(view.getContext());
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			int m = Math.round(getResources().getDimension(R.dimen.info_element_margin));
			lp.setMargins(m, m, m, m);
			lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
			lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			lp.addRule(RelativeLayout.ABOVE, R.id.info_fragment_aktionen);
			lv.setLayoutParams(lp);
			rl.addView(lv); // TODO: rl ist null??
			l = lv;
		} else {
//			HorizontalListView hlv = (HorizontalListView) view.findViewById(R.id.info_fragment_spieler_liste);
//			l = hlv;
			HorizontalListView hlv = new HorizontalListView(view.getContext());
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			int m = Math.round(getResources().getDimension(R.dimen.info_element_margin));
			lp.setMargins(m, m, m, m);
			lp.addRule(RelativeLayout.CENTER_VERTICAL);
			lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			lp.addRule(RelativeLayout.LEFT_OF, R.id.info_fragment_aktionen);
			hlv.setLayoutParams(lp);
			rl.addView(hlv);
			l = hlv;
		}
//		AdapterView<ListAdapter> l = (AdapterView) view.findViewById(R.id.info_fragment_spieler_liste); // <ListAdapter>

		ArrayList<NutzerModell> s = new ArrayList<NutzerModell>();
		for (int i = 0; i < 16; i++) {
			s.add(DominionApplication.getInstance().getManager().getEigenerNutzer());
		}
		adapter = new SpielerBildAdapter(view.getContext(), s);
		l.setAdapter(adapter);

		return view;
	}

	public void setSpiel(SpielModell spiel) {
		this.spiel = spiel;
		// TODO: spielabhängig gestalten!
//		adapter.notifyDataSetChanged();
	}
}
