package dominion.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.ui.KarteAnimation;
import dominion.ui.KartenAuslageView;
import dominion.ui.OnTouchedKartenAuslageView;

public class AusgespielteKartenFragment extends Fragment implements OnTouchedKartenAuslageView {
	private KarteAnimation animation;
	private OnTouchedKartenAuslageView touchListenerActivity;

	public AusgespielteKartenFragment() {
		// Required empty public constructor
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			animation = (KarteAnimation) activity;
			touchListenerActivity = (OnTouchedKartenAuslageView) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement KarteAnimation and OnTouchedKartenAuslageView!");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_ausgespielte_karten, container, false);

		PictureLoader kl = PictureLoader.getInstance();
		ArrayList<KarteModell> karten = new ArrayList<KarteModell>();
		karten.add(kl.createDorf());
		karten.add(kl.createDorf());
		karten.add(kl.createRatsversammlung());
		karten.add(kl.createDorf());
		karten.add(kl.createRatsversammlung());
		karten.add(kl.createDorf());
		karten.add(kl.createBibliothek());
		karten.add(kl.createDorf());
		karten.add(kl.createRatsversammlung());
		karten.add(kl.createDorf());
		karten.add(kl.createRatsversammlung());
		karten.add(kl.createDorf());

		KartenAuslageView list = (KartenAuslageView) view.findViewById(R.id.spiel_fragment_ausgespielt);
		list.setOwnHandler(this);
		list.setAnimationKarte(animation);
		list.setKarten(karten);

		return view;
	}

	@Override
	public void onTouchedKartenAuslageView(KartenAuslageView view) {
		touchListenerActivity.onTouchedKartenAuslageView(view);
	}
}
