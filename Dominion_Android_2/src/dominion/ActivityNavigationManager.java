package dominion;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import dominion.activity.LobbyActivity;
import dominion.activity.SpielActivity;
import dominion.activity.SpielvorbereitungActivity;
import dominion.activity.UserListActivity;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.types.SpielStatus;

/**
 * Klasse zum Umschalten zwischen den verschiedenen Activities per DropDown-Menü.
 * Pro Spiel, Lobby, (Profil), (Spiel erstellen??) gibt es eine Activity.
 * @author Johannes
 */
public class ActivityNavigationManager implements ActionBar.OnNavigationListener {
	public static final int LOBBY = 0;
	public static final int USER_LIST = 1;
	public static final int GAME = Integer.MIN_VALUE;
	public static final int GAMES_OFFSET = 2;

	private Context context;
	private boolean firstCall;

	public ActivityNavigationManager(Context context, ActionBar bar) {
		this.context = context;
		this.firstCall = true;
		bar.setDisplayShowTitleEnabled(false);
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		bar.setDisplayHomeAsUpEnabled(true);
		bar.setTitle(R.string.app_name);
		bar.setListNavigationCallbacks(DominionApplication.getInstance().getActivityNavigationAdapter(), this);
		bar.setSelectedNavigationItem(DominionApplication.getInstance().getActionBarSelection());
	}

	@Override
	public boolean onNavigationItemSelected(int pos, long id) {
		if (firstCall) {
			firstCall = false;
			return true;
		}
		DominionApplication app = DominionApplication.getInstance();
		if (pos == app.getActionBarSelection()) {
			return false;
		}
		if (pos == LOBBY) {
			Intent intent = new Intent(context, LobbyActivity.class);
			context.startActivity(intent);
			app.setActionBarSelection(LOBBY);
			return true;
		} else if (pos == USER_LIST) {
			Intent intent = new Intent(context, UserListActivity.class);
			context.startActivity(intent);
			app.setActionBarSelection(USER_LIST);
			return true;
		} else { // pos = some game
			Object gameObject = app.getActivityNavigationAdapter().getItem(pos);
			if (gameObject == null) {
				return false;
			}
			SpielModell game = (SpielModell) gameObject;
			if (game.getSpielStatus() == SpielStatus.SPIEL_VORBEREITUNG) {
				Intent intent = new Intent(context, SpielvorbereitungActivity.class);
				intent.putExtra(SpielvorbereitungActivity.KEY_SPIEL_ID, game.getSpielID());
				context.startActivity(intent);
				app.setActionBarSelection(pos);
				return true;
			} else if (game.getSpielStatus() == SpielStatus.SPIEL_LAEUFT) {
				Intent intent = new Intent(context, SpielActivity.class);
				intent.putExtra(SpielActivity.KEY_SPIEL_ID, game.getSpielID());
				context.startActivity(intent);
				app.setActionBarSelection(pos);
				return true;
			} else {
				return false;
			}
		}
	}
}
