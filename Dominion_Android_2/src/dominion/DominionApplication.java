package dominion;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import dominion.adapter.ActivityNavigationAdapter;
import dominion.communication.util.ThreadUtilities;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.main.DominionClientExperiment;
import dominion.miniModell.ManagerClient;

public class DominionApplication extends Application {
	
	public static int NOTIFICATION_ID_NEW_GAME = 0;
	private static String TAG = DominionApplication.class.getName();
	
	private static DominionApplication instance = null;
	
	private DominionClientExperiment client;
	private int actionBarSelection;
	private ActivityNavigationAdapter navAdapter;

	public static DominionApplication getInstance() {
		return instance;
	}

	public ManagerClient getManager() {
		return client.getClient();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				client = DominionClientExperiment.getInstance();
				return null;
			}
		}.execute();
		while(client == null) {
			ThreadUtilities.sleep(100);
		}
		PictureLoader.init(getResources());
		actionBarSelection = ActivityNavigationManager.LOBBY;
		instance = this;
		navAdapter = new ActivityNavigationAdapter(this);
	}
	
	public void stopSynchro() {
		client.stop();
	}

	public boolean istModellVorhanden() {
		boolean vorhanden;
		try {
			vorhanden = this.getManager().getModell().getKartenTypen().size() > 1;
		} catch (Exception e) {
			vorhanden = false;
		}
		return vorhanden;
	}

	public void issueNotification(String contentTitle, String contentText, Class<?> activityToLaunch) {
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
		notificationBuilder.setContentTitle(contentTitle);
		notificationBuilder.setContentText(contentText);
		notificationBuilder.setSmallIcon(R.drawable.dominion_logo_title);
		notificationBuilder.setLargeIcon(PictureLoader.getInstance().getLogo());
		notificationBuilder.setAutoCancel(true);
		if (activityToLaunch != null) {
			TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
			taskStackBuilder.addParentStack(activityToLaunch);
			Intent nextIntent = new Intent(this, activityToLaunch);
			taskStackBuilder.addNextIntent(nextIntent);
			PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			notificationBuilder.setContentIntent(pendingIntent);
		}
		NotificationManager notificationManager = (NotificationManager)
				getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(
				Application.class.getName(),
				NOTIFICATION_ID_NEW_GAME,
				notificationBuilder.build());
	}
	
	public int getActionBarSelection() {
		return actionBarSelection;
	}
	
	public void setActionBarSelection(int actionBarSelection) {
		this.actionBarSelection = actionBarSelection;
	}
	
	public ActivityNavigationAdapter getActivityNavigationAdapter() {
		return navAdapter;
	}
}