package dominion.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.Toast;
import dominion.ActivityNavigationManager;
import dominion.DominionApplication;
import dominion.dominion_android_2.R;
import dominion.fragment.InfoFragment;
import dominion.fragment.ShopFragment;
import dominion.fragment.SpielFragment;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.miniModell.modell.elements.KartenTypModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.presenter.KartenAuswahlStrg;
import dominion.ui.KarteAnimation;
import dominion.ui.KartenAuslageView;
import dominion.ui.OnTouchedKartenAuslageView;
import dominion.ui.SpielOverlay;

public class SpielActivity extends Activity implements KarteAnimation, OnTouchedKartenAuslageView, KartenAuswahlStrg {
	public static final String KEY_SPIEL_ID = 
			SpielActivity.class.getName()
			+ ".spielID";
	private static final String TAG = SpielActivity.class.getName();

	private SpielModell spiel;
	private SpielOverlay overlay;

	private ActionMode modeGlobal = null;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_SPIEL_ID, this.spiel.getSpielID());
	}

	private ActionMode.Callback callback = new ActionMode.Callback() {		
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			// diese Methode wird bei jedem Schließen der Bar aufgerufen
			SpielActivity.this.modeGlobal = null;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// wird anscheinend nur einmal aufgerufen zu Beginn, wenn die ActionBar (neu) angezeigt werden soll
			MenuInflater in = mode.getMenuInflater();
			in.inflate(R.menu.ausgespielt, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switchContextualAusgespielt(item.getItemId());
			return true;
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spiel);

		// DropDown-Menü setzen:
		new ActivityNavigationManager(getActionBarThemedContextCompat(), getActionBar());

		// Overlay holen
		overlay = (SpielOverlay) findViewById(R.id.shop_fragment_image);

		// Contextual Menus setzen
		FrameLayout fr = (FrameLayout) findViewById(R.id.spiel_framelayout_aus);
		fr.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return SpielActivity.this.onActivateContextualAusgespielt();
			}
		});

		// Ansicht Shop-Spiel korrigieren ("kleiner Hack")
		this.switchSpielShop(false);
		this.switchSpielShop(false);

		// auf Änderungen beim Wechseln zwischen Frames horchen
		getFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {
				// die ActionBar aktualisieren
				invalidateOptionsMenu();
			}
		});

		// Ansicht Karten-Log korrigieren ("kleiner Hack")
		this.switchContextualAusgespielt(R.id.ausgespielt_karten);

		// Inhalt aus Modell holen
		String spielID = null;
		if (savedInstanceState != null && savedInstanceState.containsKey(KEY_SPIEL_ID)) {
			spielID = savedInstanceState.getString(KEY_SPIEL_ID);
		}
		if (spielID == null) {
			if (getIntent() == null || getIntent().getStringExtra(KEY_SPIEL_ID) == null) {
				Log.e(TAG, "Intent and savedInstanceState did not provide gameID");
				finish();
			} else {
				spielID = getIntent().getStringExtra(KEY_SPIEL_ID);
			}
		}
		spiel = DominionApplication.getInstance().getManager().getModell().getSpiel(spielID);

		ShopFragment shopFrag = (ShopFragment) getFragmentManager().findFragmentById(R.id.spiel_fragment_shop);
		shopFrag.setSpiel(spiel);
		SpielFragment spielFrag = (SpielFragment) getFragmentManager().findFragmentById(R.id.spiel_fragment_spiel);
		spielFrag.setSpiel(spiel);
		InfoFragment infoFrag = (InfoFragment) getFragmentManager().findFragmentById(R.id.spiel_fragment_info);
		infoFrag.setSpiel(spiel);

		KartenAuslageView hand = (KartenAuslageView) findViewById(R.id.spiel_fragment_hlist);
		hand.starteAuswahl(this, new ArrayList<KarteModell>(), 0, 3, true);
	}

	private boolean onActivateContextualAusgespielt() {
		if (modeGlobal == null) {
			modeGlobal = startActionMode(callback);
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.spiel, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		Fragment spiel = getFragmentManager().findFragmentById(R.id.spiel_fragment_spiel);
		if (spiel.isVisible()) {
			menu.findItem(R.id.spiel_menu_change).setIcon(R.drawable.buy_icon);
		} else {
			menu.findItem(R.id.spiel_menu_change).setIcon(R.drawable.cards_icon);			
		}		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.spiel_menu_fertig) {
			Toast.makeText(this, "fertig!", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (item.getItemId() == R.id.spiel_menu_change) {
			switchSpielShop(true);
			return true;
		}
		if (item.getItemId() == android.R.id.home) { // Up-Button
			Intent parent = new Intent(this, LobbyActivity.class);
			parent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // muss / soll so sein
			startActivity(parent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void switchSpielShop(boolean addToBackStack) {
		FragmentManager m = getFragmentManager();
		FragmentTransaction tr = m.beginTransaction();

		Fragment spiel = m.findFragmentById(R.id.spiel_fragment_spiel);
		Fragment shop = m.findFragmentById(R.id.spiel_fragment_shop);

		if (spiel.isVisible()) {
			tr.show(shop);
			tr.hide(spiel);
			overlay = (SpielOverlay) findViewById(R.id.shop_fragment_image);
		} else {
			tr.show(spiel);
			tr.hide(shop);
			overlay = (SpielOverlay) findViewById(R.id.spiel_fragment_image);
		}
		if (addToBackStack) {
			tr.addToBackStack(null); // muss!!
		}
		tr.commit();

		invalidateOptionsMenu(); // triggert onPrepareOptionsMenu() -> Symbol wird umgestellt!
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	@Override
	public void showKarteBig(KarteModell karte) {
		overlay.showKarteBig(karte);
	}

	@Override
	public void showKartenTypBig(KartenTypModell typ) {
		overlay.showKartenTypBig(typ);
	}

	@Override
	public void onTouchedKartenAuslageView(KartenAuslageView view) {
		this.onActivateContextualAusgespielt();
	}

	private void switchContextualAusgespielt(int item) {
		View aus = findViewById(R.id.spiel_fragment_ausgespielt);
		View log = findViewById(R.id.spiel_fragment_log);

		// TODO: dies auch in Back-History einpflegen ?? -> erstmal nein!
		switch (item) {
		case R.id.ausgespielt_karten:
			aus.setVisibility(View.VISIBLE);
			log.setVisibility(View.INVISIBLE);
			//mode.finish(); // schließt die ActionBar!
			break;
		case R.id.ausgespielt_log:
			aus.setVisibility(View.INVISIBLE);
			log.setVisibility(View.VISIBLE);
			//mode.finish(); // schließt die ActionBar!
			break;
		}
	}

	@Override
	public void onAuswahlFertig(List<KarteModell> ausgewaehlte) {
		// TODO Auto-generated method stub
		
	}
}
