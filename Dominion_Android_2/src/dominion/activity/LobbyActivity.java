package dominion.activity;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import dominion.ActivityNavigationManager;
import dominion.dominion_android_2.R;
import dominion.fragment.LobbyInputFragment;

public class LobbyActivity extends Activity {

	private int selection;
	private ActivityNavigationManager navManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null && savedInstanceState.containsKey("selection"))
			this.selection = savedInstanceState.getInt("selection");
		else
			this.selection = -1;

		setContentView(R.layout.activity_lobby);
		// DropDown-Menü setzen:
		navManager = new ActivityNavigationManager(getActionBarThemedContextCompat(), getActionBar());
		// Adapter für Spiele in der Lobby setzen
		LobbyInputFragment inputFragment = (LobbyInputFragment) getFragmentManager()
				.findFragmentById(R.id.lobby_input_fragment);
		inputFragment.setAdaptersAndData(this);



		getActionBar().setDisplayHomeAsUpEnabled(false); // Up-Button hier ausschalten
	}

	/**
	 * TODO implement properly
	 * The way this is implemented at the moment it only prevents the application
	 * from crashing when the screen orientation changes.
	 * It discards any state changes that have been made.
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.clear();
		outState.putInt("selection", this.selection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lobby, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.lobby_menu_create_game) {
			Intent intent = new Intent(this, CreateGameActivity.class);
			this.startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	public int getSelection() {
		return this.selection;
	}

	public void setSelection(int selection) {
		this.selection = selection;
	}
	
	public ActivityNavigationManager getActivityNavigationManager() {
		return navManager;
	}
}
