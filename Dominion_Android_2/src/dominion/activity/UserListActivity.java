package dominion.activity;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import dominion.ActivityNavigationManager;
import dominion.adapter.UserListAdapter;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.NutzerModell;

public class UserListActivity extends Activity {
	public static final String KEY_STARTED_FOR_RESULT =
			UserListActivity.class.getName()
			+ ".startedForResult";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_list);
		final ListView userList = (ListView) findViewById(R.id.user_list);
		userList.setAdapter(new UserListAdapter(this));
		if (getIntent().getBooleanExtra(KEY_STARTED_FOR_RESULT, false)) {
			userList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position,
						long id) {
					Intent result = new Intent();
					NutzerModell selectedUser = (NutzerModell) userList.getAdapter().getItem(position);
					int userID = selectedUser.getNutzerID();
					result.putExtra(SpielvorbereitungActivity.KEY_SELECTED_USER_ID, userID);
					setResult(RESULT_OK, result);
					finish();
				}
			});
		}

		// DropDown-Menü setzen:
		new ActivityNavigationManager(getActionBarThemedContextCompat(), getActionBar());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO: diese und die folgende Methode müssten eigentlich in alle Activities gesetzt werden!!
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user_list, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			Context context = this;
			Intent intent = new Intent(context, SpielvorbereitungActivity.class);
			//			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
			finish();
		}
		if (item.getItemId() == android.R.id.home) { // Up-Button
			Intent parent = new Intent(this, LobbyActivity.class);
			parent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}
}
