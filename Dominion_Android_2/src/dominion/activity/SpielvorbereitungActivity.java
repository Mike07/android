package dominion.activity;

import java.math.BigInteger;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import dominion.ActivityNavigationManager;
import dominion.DominionApplication;
import dominion.adapter.SpielerAdapter;
import dominion.dominion_android_2.R;
import dominion.fragment.AISelectionDialogFragment;
import dominion.miniModell.klassifikation.callback.AbstractCallback;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.observer.update.SpielObserver;
import dominion.miniModell.modell.types.SpielStatus;

public class SpielvorbereitungActivity extends Activity implements SpielObserver {
	public static final String KEY_SPIEL_ID = 
			SpielvorbereitungActivity.class.getName()
			+ ".spielID";
	public static final String KEY_SELECTED_USER_ID = 
			SpielvorbereitungActivity.class.getName()
			+ ".selectedUserID";
	static final String NO_GAME = BigInteger.valueOf(-1).toString();
	public static final int RQ_CODE_SELECT_PLAYER = 1;
	private static final String TAG = SpielvorbereitungActivity.class.getName();

	private SpielModell spiel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.v(TAG, "onCreate called");
		super.onCreate(savedInstanceState);
		String spielID = null;
		if (savedInstanceState != null) {
			spielID = savedInstanceState.getString(KEY_SPIEL_ID, NO_GAME);
			if (spielID.equals(NO_GAME)) {
				finish();
				return;
			}
		} else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(KEY_SPIEL_ID)) {
			spielID = (String) getIntent().getExtras().get(KEY_SPIEL_ID);
		} else {
			finish();
			return;
		}
		Log.v(TAG, "onCreate got gameID " + spielID);

		spiel = DominionApplication.getInstance().getManager().getModell().getSpiel(spielID);
		setContentView(R.layout.activity_spielvorbereitung);

		// DropDown-Menü setzen:
		new ActivityNavigationManager(getActionBarThemedContextCompat(), getActionBar());

		TextView title = (TextView) findViewById(R.id.vorbereitung_title);
		title.setText(String.format(title.getText().toString(), spiel.getName()));

		ListView list = (ListView) findViewById(R.id.vorbereitung_spieler_liste);
		list.setAdapter(new SpielerAdapter(this, spiel));

		Button addAI = (Button) findViewById(R.id.vorbereitung_button_add_ki);
		if (DominionApplication.getInstance().getManager().getEigenerNutzer().equals(spiel.getHostNutzer())) {
			addAI.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AISelectionDialogFragment dialog = new AISelectionDialogFragment();
					dialog.show(getFragmentManager(), "AISelectionDialog");
//					Main.getInstance().getManager().getSpieleVerwaltungService()...
				}
			});
		} else {
			addAI.setEnabled(false);
		}

		Button addPlayer = (Button) findViewById(R.id.vorbereitung_button_add_spieler);
		addPlayer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Context context = SpielvorbereitungActivity.this;
				Intent intent = new Intent(context, UserListActivity.class);
				intent.putExtra(UserListActivity.KEY_STARTED_FOR_RESULT, true);
				SpielvorbereitungActivity.this.startActivityForResult(intent, RQ_CODE_SELECT_PLAYER);
			}
		});

		// Spiel starten:
		Button start = (Button) findViewById(R.id.vorbereitung_button_start);
		start.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DominionApplication.getInstance().getManager().getSpieleVerwaltungService().starteSpiel(spiel, new AbstractCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Log.e(TAG, "unable to start game " + spiel.getSpielID());
						Log.e(TAG, Log.getStackTraceString(caught));
					}
					@Override
					public void onSuccess(Void result) {
						Log.d(TAG, "successfully started game " + spiel.getSpielID());
					}
				});
			}
		});

		// über Änderungen benachrichtigt werden!
		spiel.addSpielObserver(this, true);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_SPIEL_ID, spiel.getSpielID());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RQ_CODE_SELECT_PLAYER) {
			if (resultCode == RESULT_OK) {
				int userID = data.getExtras().getInt(KEY_SELECTED_USER_ID, Integer.MIN_VALUE);
				if (userID >= 0) {
					// TODO invite player
					Toast.makeText(this, "Invited player with id " + userID, Toast.LENGTH_SHORT).show();
				}
			} else {
				// no player selected, nothing to do
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.spielvorbereitung, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) { // Up-Button
			Intent parent = new Intent(this, LobbyActivity.class);
			parent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	@Override
	public void updateAll(SpielModell spiel) {
		this.updateSpielStatus(spiel);
	}

	@Override
	public void updateAktuelleKartenTypen(SpielModell spiel) {
		// bleibt leer
	}

	@Override
	public void updateAddBeobachter(SpielModell spiel, NutzerModell beobachter) {
		// bleibt leer
	}

	@Override
	public void updateDeleteBeobachter(SpielModell spiel, NutzerModell beobachter) {
		// bleibt leer
	}

	@Override
	public void updateAktuellerSpieler(SpielModell spiel) {
		// bleibt leer
	}

	@Override
	public void updatePhaseStatus(SpielModell spiel) {
		// bleibt leer
	}

	@Override
	public void updateSpielStatus(SpielModell spiel) {
		if (spiel.getSpielStatus() == SpielStatus.SPIEL_BEENDET) {
			finish();
		}
		if (spiel.getSpielStatus() == SpielStatus.SPIEL_LAEUFT) {
			// TODO: evtl. Spiel-Sicht anzeigen??
			finish();
		}
		if (spiel.getSpielStatus() == SpielStatus.SPIEL_VORBEREITUNG) {
			// bleibt leer
		}
	}
}
