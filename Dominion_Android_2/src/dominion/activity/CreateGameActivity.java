package dominion.activity;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import dominion.DominionApplication;
import dominion.adapter.CardSetsSpinnerAdapter;
import dominion.dominion_android_2.R;
import dominion.miniModell.klassifikation.callback.AbstractCallback;
import dominion.miniModell.modell.elements.KartenSetModell;
import dominion.miniModell.modell.elements.KartenTypModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.types.KiOption;

public class CreateGameActivity extends Activity {
	private CardSetsSpinnerAdapter cardAdapter;

	private EditText name;
	private EditText maxSpieler;
	private CheckBox kiOption;
	private Spinner cardSetsSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_game);

		cardAdapter = new CardSetsSpinnerAdapter(this);

		// set List-Adapter
		final ListView cardSetDetailsList = (ListView) findViewById(R.id.card_set_items_list);
		final ArrayAdapter<String> cslAdapter = new ArrayAdapter<String>(
				this,
				R.layout.general_purpose_text_medium,
				new ArrayList<String>()
				);
		if (!cardAdapter.isEmpty()) {
			cslAdapter.addAll(this.toCards(cardAdapter.getSet(0)));
		}
		cardSetDetailsList.setAdapter(cslAdapter);

		// set Spinner-Adapter
		cardSetsSpinner = (Spinner) findViewById(R.id.card_set_spinner);
		cardSetsSpinner.setAdapter(cardAdapter);
		cardSetsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				cslAdapter.clear();
				cslAdapter.addAll(CreateGameActivity.this.toCards(cardAdapter.getSet(position)));
				cslAdapter.notifyDataSetChanged();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				cslAdapter.clear();
				if (!cardAdapter.isEmpty()) {
					cslAdapter.addAll(CreateGameActivity.this.toCards(cardAdapter.getSet(0)));
				}
				cslAdapter.notifyDataSetChanged();
			}
		});

		// Views zum Herausholen der Daten merken!
		name = (EditText) findViewById(R.id.game_name_input);
		maxSpieler = (EditText) findViewById(R.id.max_players_input);
		kiOption = (CheckBox) findViewById(R.id.ai_option_input);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO: diese und die folgende Methode müssten eigentlich in alle Activities gesetzt werden!!
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.create_game, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.create_game_menu_fertig) {
			String text = name.getText().toString();
			int max = Integer.parseInt(maxSpieler.getText().toString());
			KiOption option = KiOption.BEENDE_SPIEL;
			if (kiOption.isChecked()) {
				option = KiOption.ERSETZE_SPIELER_DURCH_KI;
			}
			DominionApplication.getInstance().getManager().getSpieleVerwaltungService().erstelleNeuesSpiel(text, 1, max, option,
					new AbstractCallback<SpielModell>() {
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
						}

						@Override
						public void onSuccess(SpielModell result) {
							new AsyncTask<SpielModell, Void, SpielModell>() {
								@Override
								protected SpielModell doInBackground(SpielModell... params) {
									return params[0];
								}

								@Override
								protected void onPostExecute(SpielModell result) {
									Toast.makeText(CreateGameActivity.this, "Spiel erstellt: " + result.getSpielID(), Toast.LENGTH_LONG).show();
									// TODO: dies neue Spiel in Navigation und Spielvorbereitung "einbauen"
									CreateGameActivity.this.sendSetToServer(result);
								}
							}.execute(result);
						}
			});
		}
		if (item.getItemId() == android.R.id.home) { // Up-Button
			Intent parent = new Intent(this, LobbyActivity.class);
			parent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void sendSetToServer(final SpielModell neues) {
		KartenSetModell set = CreateGameActivity.this.cardAdapter.getSet(
				CreateGameActivity.this.cardSetsSpinner.getSelectedItemPosition());
		DominionApplication.getInstance().getManager().getSpieleVerwaltungService().setKarten(neues, set.getKartenTypen(), new AbstractCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
				Intent intent = new Intent(CreateGameActivity.this, SpielvorbereitungActivity.class);
				intent.putExtra(SpielvorbereitungActivity.KEY_SPIEL_ID, neues.getSpielID());
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
		});
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	private ArrayList<String> toCards(KartenSetModell set) {
		ArrayList<String> res = new ArrayList<String>();
		for (KartenTypModell typ : set.getKartenTypen()) {
			res.add(typ.getName());
		}
		return res;
	}
}
