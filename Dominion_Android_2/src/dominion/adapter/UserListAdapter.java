package dominion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import dominion.DominionApplication;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.modell.elements.RechteModell;
import dominion.miniModell.modell.observer.addDelete.NutzerAddDeleteObserver;
import dominion.miniModell.modell.observer.update.NutzerObserver;
import dominion.miniModell.modell.observer.update.RechteObserver;

public class UserListAdapter extends BaseAdapter implements NutzerAddDeleteObserver, NutzerObserver, RechteObserver {
	private Context context;
	private List<NutzerModell> users;

	public UserListAdapter(Context context) {
		this.context = context;
		this.users = new ArrayList<NutzerModell>();
		DominionApplication.getInstance().getManager().getModell().addNutzerAddDeleteObserver(this, true);
	}

	@Override
	public int getCount() {
		return users.size();
	}

	@Override
	public Object getItem(int position) {
		return users.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = LayoutInflater.from(context).inflate(R.layout.player_list_item, parent, false);
		NutzerModell user = users.get(position);
		// TODO Nutzerbild-String in Avatar-Bild-Pfad übersetzen
		ImageView avatar = (ImageView) convertView.findViewById(R.id.player_avatar);
		Bitmap bitmap = PictureLoader.getInstance().getAvatarGross(user);
		avatar.setImageBitmap(bitmap);

		TextView nicknameAndStatus = (TextView) convertView.findViewById(R.id.player_nick_and_status);
		nicknameAndStatus.setText(userNickAndStatus(user));
		
		TextView fullName = (TextView) convertView.findViewById(R.id.player_full_name);
		fullName.setText(user.getVorname() + " " + user.getNachname());
		
		return convertView;
	}
	
	public static String userNickAndStatus(NutzerModell user) {
		return user.getNickname()
				+ " ("
				+ onlineStatus(user)
				+ ")"
				// TODO remove the ID from this String (debug)
				+ " ["
				+ user.getNutzerID()
				+ "]";
	}
	
	public static String onlineStatus(NutzerModell user) {
		if (user.getNutzerRechte().isOnline())
			return "Online"; // TODO: Strings auslagern!!
		else
			return "Offline";
	}

	@Override
	public void addNutzer(NutzerModell user) {
		new AsyncTask<NutzerModell, Void, NutzerModell>() {
			@Override
			protected NutzerModell doInBackground(NutzerModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(NutzerModell result) {
				UserListAdapter.this.users.add(result);
				UserListAdapter.this.notifyDataSetChanged();
				result.addNutzerObserver(UserListAdapter.this, false);
				result.getNutzerRechte().addRechteObserver(UserListAdapter.this, false);
			}		
		}.execute(user);
	}

	@Override
	public void deleteNutzer(NutzerModell user) {
		new AsyncTask<NutzerModell, Void, NutzerModell>() {
			@Override
			protected NutzerModell doInBackground(NutzerModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(NutzerModell result) {
				// TODO kontrollieren: nicht mehr vorhandener Index ...?
				UserListAdapter.this.users.remove(result);
				UserListAdapter.this.notifyDataSetChanged();
				result.removeNutzerObserver(UserListAdapter.this);
				result.getNutzerRechte().removeRechteObserver(UserListAdapter.this);
			}		
		}.execute(user);
	}

	private void updateNutzer(NutzerModell nutzer) {
		new AsyncTask<NutzerModell, Void, NutzerModell>() {
			@Override
			protected NutzerModell doInBackground(NutzerModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(NutzerModell result) {
				UserListAdapter.this.notifyDataSetChanged();
			}
		}.execute(nutzer);
	}

	@Override
	public void updateAll(NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateMitgliedID(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateBild(NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateVorname(NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateNachname(NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateNickname(NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateEMail(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateWohnort(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateGeburtsdatum(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateZuletztOnline(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateRegistriertAm(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateSpieleGesamt(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateSpieleGewonnen(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateBewertung(NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateAll(RechteModell rechte, NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateSpielErstellen(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateSpielBeobachten(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateFremdesMitgliedDatenAendern(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateFremdesSpielLoeschen(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateFremdesMitgliedDatenSehen(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateFremdesMitgliedSperren(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateFremdesMitgliedEntsperren(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateOnline(RechteModell rechte, NutzerModell nutzer) {
		this.updateNutzer(nutzer);
	}

	@Override
	public void updateEingeloggt(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateAdmin(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateGesperrt(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}

	@Override
	public void updateAktiviert(RechteModell rechte, NutzerModell nutzer) {
		// bleibt leer
	}
}
