package dominion.adapter;

import java.util.List;

import dominion.miniModell.modell.elements.SpielModell;

public interface LobbyDetailsAdapter {
	
	public List<SpielModell> getGames();

}
