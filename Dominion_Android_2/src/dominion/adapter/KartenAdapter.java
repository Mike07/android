package dominion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.presenter.KartenAuswahlStrg;
import dominion.presenter.KartenAuswahlView;
import dominion.ui.KarteAnimation;
import dominion.ui.karte.KarteView;

/**
 * Adapter für Karten, der für ListViews und Spinner eingesetzt werden kann.
 * - initial müssen schon Karten und Animation gesetzt werden
 * @author Johannes
 */
public class KartenAdapter extends BaseAdapter implements KartenAuswahlView, OnTouchListener {
	private ArrayList<KarteModell> karten;
	private KarteAnimation animation;
	private int rowsCount;

	private KartenAuswahlStrg strg;
	private List<KarteModell> waehlbare;
	private int min;
	private int max;
	private boolean reihenfolgeWaehlbar;
	private boolean auswaehlen;
	private List<KarteModell> ausgewaehlte;

	public KartenAdapter() {
		super();
		this.auswaehlen = false;
		this.setRowsCount(1);
	}

	public ArrayList<KarteModell> getKarten() {
		return karten;
	}

	public void setKarten(ArrayList<KarteModell> karten) {
		this.karten = karten;
	}

	public KarteAnimation getAnimation() {
		return animation;
	}

	public void setAnimation(KarteAnimation animation) {
		this.animation = animation;
	}

	public int getRowsCount() {
		return rowsCount;
	}

	public void setRowsCount(int rowsCount) {
		if (rowsCount <= 0) {
			throw new IllegalArgumentException("Anzahl Zeilen muss größer gleich 1 sein!");
		}
		if (rowsCount != this.rowsCount) {
			this.rowsCount = rowsCount;
			this.notifyDataSetInvalidated();
		}
	}

	@Override
	public int getCount() {
		int half = this.karten.size() / rowsCount;
		if (this.karten.size() % rowsCount > 0) {
			half++;
		}
		return half;
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0; // oder position ??
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = parent.getContext();

		if (rowsCount == 1) { // Optimierung für nur eine Zeile (kein unnötiges LinearLayout!)
			KarteView k = new KarteView(context);
			KarteModell m = this.karten.get(position);
			k.setOnTouchListener(this);
			k.setAnimation(animation);
			k.setKarte(m);
//			if (auswaehlen) { // TODO
//				if (waehlbare.contains(m)) {
//					k.setAktiv(true);
//				} else {
//					k.setAktiv(false);
//				}
//			}
			return k;
		}

		int half = this.getCount();

		LinearLayout v = new LinearLayout(context);
		v.setOrientation(LinearLayout.VERTICAL);

		for (int i = 0; i < rowsCount; i++) {
			KarteView k = new KarteView(context);
			v.addView(k);
			k.setOnTouchListener(this);
			k.setAnimation(animation);

			int index = i * half + position;

			if (index >= this.karten.size()) {
				k.setKarte(null);				
			} else {
				KarteModell m = this.karten.get(index);
				k.setKarte(m);
//				if (auswaehlen) { // TODO
//					if (waehlbare.contains(m)) {
//						k.setAktiv(true);
//					} else {
//						k.setAktiv(false);
//					}
//				}
			}
		}

		return v;
	}

	@Override
	public void starteAuswahl(KartenAuswahlStrg strg, List<KarteModell> waehlbare, int min, int max, boolean reihenfolgeWaehlbar) {
		this.strg = strg;
		this.auswaehlen = true;
		this.ausgewaehlte = new ArrayList<KarteModell>();

		this.waehlbare = waehlbare;
		this.min = min;
		this.max = max;
		this.reihenfolgeWaehlbar = reihenfolgeWaehlbar;

		this.notifyDataSetChanged();
	}

	@Override
	public void beendeAuswahl() {
		this.auswaehlen = false;
		this.notifyDataSetChanged();
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if (!auswaehlen) {
			return false;
		}
		System.out.println("Auswahl!!!!!!!!!!!!!!!!");
		KarteView k = (KarteView) view;
//		if (ausgewaehlte.contains(k.getKarte())) {
//			k.setChecked(false);
//			k.setCheckedNumber(checkedNumber)
//		} else {
//			
//		}
		System.out.println(k.isChecked());
		k.setChecked(!k.isChecked());
		System.out.println(k.isChecked());
		this.notifyDataSetChanged();
		return false;
	}
}
