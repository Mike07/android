package dominion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import dominion.ActivityNavigationManager;
import dominion.DominionApplication;
import dominion.activity.SpielActivity;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.elements.SpielerModell;
import dominion.miniModell.modell.observer.addDelete.SpielAddDeleteObserver;
import dominion.miniModell.modell.observer.addDelete.SpielerAddDeleteObserver;
import dominion.miniModell.modell.observer.update.SpielObserver;
import dominion.miniModell.modell.types.SpielStatus;

public class ActivityNavigationAdapter extends BaseAdapter implements SpielAddDeleteObserver, SpielObserver, SpielerAddDeleteObserver {
	private Context context;
	private List<SpielModell> meineSpiele;
	private static final String TAG = ActivityNavigationAdapter.class.getName();

	public ActivityNavigationAdapter(Context context) {
		super();
		this.context = context;

		meineSpiele = new ArrayList<SpielModell>();
		DominionApplication.getInstance().getManager().getModell().addSpielAddDeleteObserver(this, true);
	}
	
	public int getNavPosition(SpielModell spiel) {
		for (int i = 0; i < meineSpiele.size(); i++) {
			if (meineSpiele.get(i).equals(spiel)) {
				return i + ActivityNavigationManager.GAMES_OFFSET;
			}
		}
		return -1;
	}

	public SpielModell getSpiel(int index) {
		return meineSpiele.get(index);
	}

	@Override
	public int getCount() {
		return ActivityNavigationManager.GAMES_OFFSET + meineSpiele.size();
	}

	@Override
	public Object getItem(int position) {
		if (position < ActivityNavigationManager.GAMES_OFFSET) {
			return null;
		} else {
			return meineSpiele.get(position - ActivityNavigationManager.GAMES_OFFSET);
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.general_purpose_text_large, parent, false);
		} // TODO: eigene XML evtl. durch vorgefertigte ersetzen?
		TextView textView = (TextView) convertView.findViewById(R.id.general_purpose_textfield_large);
		String text = "";
		if (position == ActivityNavigationManager.LOBBY) {
			text = context.getResources().getString(R.string.dropdown_lobby);
		} else if (position == ActivityNavigationManager.USER_LIST) {
			text = context.getResources().getString(R.string.dropdown_user_list);
		} else {
			Log.d("ActivityNavigation", "position: " + position + ", spiele.size(): " + meineSpiele.size());
			text = meineSpiele.get(position - ActivityNavigationManager.GAMES_OFFSET).getName();
		}
		textView.setText(text);
		return textView;
	}

	private void update(SpielModell spiel, final boolean add) {
//		new AsyncTask<SpielModell, Void, SpielModell>() {
//			@Override
//			protected SpielModell doInBackground(SpielModell... params) {
//				return params[0];
//			}
//
//			@Override
//			protected void onPostExecute(SpielModell result) {
//				if (add && meineSpiele.contains(result)) {
//					return;
//				}
//				if (!add && !meineSpiele.contains(result)) {
//					return;
//				}
//				Log.d("ActivityAdapter", "(vorher) add: " + add + " spiel: " + result.getSpielID() + " spiele.size(): " + meineSpiele.size());
//				if (add) {
//					ActivityAdapter.this.meineSpiele.add(result);
//				} else {
//					ActivityAdapter.this.meineSpiele.remove(result);
//				}
//				Log.d("ActivityAdapter", "(nachher) add: " + add + " spiel: " + result.getSpielID() + " spiele.size(): " + meineSpiele.size());
//				ActivityAdapter.this.notifyDataSetChanged();
//			}
//		}.execute(spiel);
		if ((add && meineSpiele.contains(spiel)) || (!add && !meineSpiele.contains(spiel)))
			return;
		Log.d(TAG, "(vorher) add: " + add + " spiel: " + spiel.getSpielID() + " spiele.size(): " + meineSpiele.size());
		if (add) {
			meineSpiele.add(spiel);
		} else {
			meineSpiele.remove(spiel);
		}
		Log.d(TAG, "(nachher) add: " + add + " spiel: " + spiel.getSpielID() + " spiele.size(): " + meineSpiele.size());
	}

	@Override
	public void addSpiel(SpielModell spiel) {
		Log.v(TAG, "addSpiel called");
		spiel.addSpielObserver(this, true);
		spiel.addSpielerAddDeleteObserver(this, true);
	}

	@Override
	public void deleteSpiel(SpielModell spiel) {
		this.update(spiel, false);
		spiel.removeSpielObserver(this);
	}

	@Override
	public void updateAll(SpielModell spiel) {
		Log.v(TAG, "updateAll called");
		// nicht relevante Spiele ignorieren
		if (spiel.getSpielStatus() == SpielStatus.SPIEL_BEENDET) {
			return;
		}
		// Host?
		NutzerModell eigenerNutzer = DominionApplication.getInstance().getManager().getEigenerNutzer();
		if (eigenerNutzer == null)
			Log.e(TAG, "eigener Nutzer ist null!");
		if (spiel.getHostNutzer().equals(eigenerNutzer)) {
			this.update(spiel, true);
			return;
		}
		// Beobachter?
		if (spiel.getBeobachterNutzer().contains(DominionApplication.getInstance().getManager().getEigenerNutzer())) {
			this.update(spiel, true);
		}
		// Spieler?
		for (SpielerModell sp : spiel.getSpieler()) {
			if (sp == null) {
				Log.d(TAG, "Spieler ist null!");
				continue;
			}
			if (sp.getNutzer() == null) {
				Log.d(TAG, "Spieler.getNutzer() liefert null!");
				continue;
			}
			if (sp.getNutzer().equals(DominionApplication.getInstance().getManager().getEigenerNutzer())) {
				this.update(spiel, true);
				return;
			}
		}
	}

	@Override
	public void updateAktuelleKartenTypen(SpielModell spiel) {
		// bleibt leer
	}

	@Override
	public void updateAddBeobachter(SpielModell spiel, NutzerModell beobachter) {
		if (beobachter.equals(DominionApplication.getInstance().getManager().getEigenerNutzer())) {
			this.update(spiel, true);
		}
	}

	@Override
	public void updateDeleteBeobachter(SpielModell spiel, NutzerModell beobachter) {
		if (beobachter.equals(DominionApplication.getInstance().getManager().getEigenerNutzer())) {
			this.update(spiel, false);
		}
	}

	@Override
	public void updateAktuellerSpieler(SpielModell spiel) {
		// bleibt leer
	}

	@Override
	public void updatePhaseStatus(SpielModell spiel) {
		// bleibt leer
	}

	@Override
	public void updateSpielStatus(SpielModell spiel) {
		if (spiel.getSpielStatus() == SpielStatus.SPIEL_BEENDET) {
			this.update(spiel, false);
		} else if (spiel.getSpielStatus() == SpielStatus.SPIEL_LAEUFT) {
			SpielModell game = (SpielModell) getItem(DominionApplication.getInstance().getActionBarSelection());
			if (spiel.equals(game)) {
				Intent intent = new Intent(context, SpielActivity.class);
				intent.putExtra(SpielActivity.KEY_SPIEL_ID, game.getSpielID());
				// TODO fix this shit
				context.startActivity(intent);
			}
		}
	}

	@Override
	public void addSpieler(SpielerModell spieler) {
		NutzerModell eigenerNutzer = DominionApplication.getInstance().getManager().getEigenerNutzer();
		if (eigenerNutzer == null) {
			Log.e(TAG, "Eigener Nutzer ist null!");
		} else if (spieler.getNutzer().equals(eigenerNutzer)) {
			this.update(spieler.getSpiel(), true);			
		}
	}

	@Override
	public void deleteSpieler(SpielerModell spieler) {
		NutzerModell eigenerNutzer = DominionApplication.getInstance().getManager().getEigenerNutzer();
		if (eigenerNutzer == null) {
			Log.e(TAG, "Eigener Nutzer ist null!");
		} else if (spieler.getNutzer().equals(eigenerNutzer)) {
			this.update(spieler.getSpiel(), false);			
		}
	}
}
