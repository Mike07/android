package dominion.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KartenStapelModell;
import dominion.ui.KarteAnimation;
import dominion.ui.KarteToast;
import dominion.ui.karte.VorratStapelView;

public class SimpleShopAdapter extends BaseAdapter {
	private List<KartenStapelModell> stapel;
	private KarteAnimation animation;
	private LayoutInflater inflater;

	
	public SimpleShopAdapter(Context context, List<KartenStapelModell> stapel, KarteAnimation animation) {
		super();
		this.stapel = stapel;
		this.inflater = LayoutInflater.from(context);
		this.animation = animation;
	}

	@Override
	public int getCount() {
		return stapel.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0; // oder position ??
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		if (view == null) {
			view = inflater.inflate(R.layout.shop_item_parent, parent, false);
		}
		KartenStapelModell karte = stapel.get(position);

		VorratStapelView w = (VorratStapelView) view.findViewById(R.id.bild);
		w.setVorratStapel(karte);
		w.setAnimation(animation);

		TextView v = (TextView) view.findViewById(R.id.vorbereitung_item_name);
		v.setText(karte.getPlatzhalterKartenTyp().getName());

		Button u = (Button) view.findViewById(R.id.button_vorrat);
		u.setText(karte.getPlatzhalterKartenTyp().getKosten().getMuenzen() + " ");

		final View vi = view;

		u.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				KarteToast.showKarteToast(vi.getContext(), SimpleShopAdapter.this.stapel.get(position).getPlatzhalterKartenTyp());
			}
		});

		return view;
	}
}
