package dominion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import dominion.DominionApplication;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KartenSetModell;
import dominion.miniModell.modell.observer.addDelete.KartenSetAddDeleteObserver;

public class CardSetsSpinnerAdapter extends BaseAdapter implements KartenSetAddDeleteObserver {
	private LayoutInflater inflater;
	private List<KartenSetModell> cardSets;

	public CardSetsSpinnerAdapter(Context context) {
		this.inflater = LayoutInflater.from(context);

		this.cardSets = new ArrayList<KartenSetModell>();
		DominionApplication.getInstance().getManager().getModell().addKartensetAddDeleteObserver(this, true);
	}

	@Override
	public int getCount() {
		return cardSets.size();
	}

	@Override
	public Object getItem(int position) {
		return cardSets.get(position).getName();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.general_purpose_text_large, parent, false);
		}
		TextView cardSetName = (TextView) convertView.findViewById(R.id.general_purpose_textfield_large);
		cardSetName.setText(cardSets.get(position).getName());
		return cardSetName;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isEmpty() {
		return (cardSets.size() == 0);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = inflater.inflate(R.layout.general_purpose_text_large, parent, false);
		TextView cardSetName = (TextView) convertView.findViewById(R.id.general_purpose_textfield_large);
		cardSetName.setText(cardSets.get(position).getName());
		return cardSetName;
	}

	/**
	 * Liefert das KartenSet an der gewünschten Stelle im Adapter
	 * @param position
	 * @return
	 */
	public KartenSetModell getSet(int position) {
		return cardSets.get(position);
	}

	@Override
	public void addKartenset(KartenSetModell set) {
		this.cardSets.add(set);
		this.notifyDataSetChanged();
	}

	@Override
	public void deleteKartenset(KartenSetModell set) {
		// bleibt leer
	}
}
