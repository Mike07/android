package dominion.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.NutzerModell;

public class SpielerBildAdapter extends BaseAdapter {
	private ArrayList<NutzerModell> spieler;
	private LayoutInflater inflater;

	public SpielerBildAdapter(Context context, ArrayList<NutzerModell> spieler) {
		super();
		this.spieler = spieler;
		this.inflater = LayoutInflater.from(context);
	}

	public ArrayList<NutzerModell> getSpieler() {
		return spieler;
	}

	public void setSpieler(ArrayList<NutzerModell> spieler) {
		this.spieler = spieler;
	}

	@Override
	public int getCount() {
		return spieler.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0; // oder arg0 ??
	}

	@Override
	public View getView(final int index, View view, ViewGroup group) {
		final View v = inflater.inflate(R.layout.info_spieler_bild, group, false);
		ImageView i = (ImageView) v.findViewById(R.id.info_spieler_bild_image);
		i.setImageBitmap(PictureLoader.getInstance().getAvatarKlein(spieler.get(index)));
		i.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// TODO: Bild in Groß zeigen + Nickname (+ "ist (nicht) am Zug")!
				Toast.makeText(v.getContext(), SpielerBildAdapter.this.spieler.get(index).getNickname(), Toast.LENGTH_SHORT).show();
				return true;
			}
		});
		return v;
	}
}
