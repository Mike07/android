package dominion.adapter;

import static dominion.fragment.LobbyDetailsFragment.playersToMultiLineText;
import static dominion.fragment.LobbyDetailsFragment.toMultiLineText;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import dominion.ActivityNavigationManager;
import dominion.DominionApplication;
import dominion.activity.LobbyActivity;
import dominion.dominion_android_2.R;
import dominion.miniModell.klassifikation.callback.AbstractCallback;
import dominion.miniModell.modell.elements.KartenTypModell;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.elements.SpielerModell;

public class LobbyExpandableListAdapter extends BaseExpandableListAdapter implements LobbyDetailsAdapter {
	private static final String TAG = LobbyExpandableListAdapter.class.getName();
	
	private final LayoutInflater inflater;
	private final List<SpielModell> games;
	private final Context context;

	public LobbyExpandableListAdapter(Context context) {
		this.inflater = LayoutInflater.from(context);
		this.games = new ArrayList<SpielModell>();
		this.context = context;
	}
	
	public List<SpielModell> getGames() {
		return games;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup group) {
		if (view == null) {
			view = inflater.inflate(R.layout.fragment_lobby_details, group, false);
		}
		SpielModell game = games.get(groupPosition);

		TextView cardsList = (TextView) view.findViewById(R.id.lobby_details_cards_list);
		ArrayList<String> typen = new ArrayList<String>();
		for (KartenTypModell typ : game.getAktuelleKartenTypen()) {
			if (typ == null || typ.getName() == null)
				typen.add("<null>");
			else
				typen.add(typ.getName());
		}
		cardsList.setText(toMultiLineText(typen));

		TextView playersList = (TextView) view.findViewById(R.id.lobby_details_players_list);
		playersList.setText(playersToMultiLineText(game.getSpieler()));

		return view;
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.games.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this.games.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup group) {
		if (view == null) {
			view = inflater.inflate(R.layout.lobby_item_parent, group, false);
		}
		final SpielModell spiel = games.get(groupPosition);

		TextView v = (TextView) view.findViewById(R.id.lobby_parent_spielname);
		v.setText(spiel.getName());

		v = (TextView) view.findViewById(R.id.lobby_parent_beigetrene);
		v.setText(
				spiel.getSpieler().size()
				+ "/"
				+ spiel.getMaxSpielerAnzahl()
				+ " "
				+ context.getResources().getString(R.string.players)
		);

		Button b = (Button) view.findViewById(R.id.lobby_parent_button);
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				joinGame(spiel);
			}
		});

		return view;
	}

	private void joinGame(final SpielModell spiel) {
		DominionApplication.getInstance().getManager().getSpieleVerwaltungService().meldeSpielerAn(spiel,
				DominionApplication.getInstance().getManager().getEigenerNutzer(),
				new AbstractCallback<SpielerModell>() {
					@Override
					public void onFailure(Throwable caught) {
						Log.e(TAG, "unable to join game " + spiel.getSpielID());
						Log.e(TAG, Log.getStackTraceString(caught));
					}

					@Override
					public void onSuccess(SpielerModell result) {
						ActivityNavigationManager navManager = 
								((LobbyActivity) context).getActivityNavigationManager();
						int position = DominionApplication.getInstance().
								getActivityNavigationAdapter().getNavPosition(spiel);
						navManager.onNavigationItemSelected(position, position);
					}
				});
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		System.out.println("data set changed!");
	}

}
