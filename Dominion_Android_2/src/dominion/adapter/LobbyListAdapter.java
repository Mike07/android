package dominion.adapter;

import java.util.ArrayList;
import java.util.List;

import dominion.ActivityNavigationManager;
import dominion.DominionApplication;
import dominion.activity.LobbyActivity;
import dominion.dominion_android_2.R;
import dominion.miniModell.klassifikation.callback.AbstractCallback;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.elements.SpielerModell;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class LobbyListAdapter extends BaseAdapter implements LobbyDetailsAdapter {

	private static final String TAG = LobbyListAdapter.class.getName();
	
	private LayoutInflater inflater;
	private ArrayList<SpielModell> games;
	private Context context;

	public LobbyListAdapter(Context context) {
		this.inflater = LayoutInflater.from(context);
		this.games = new ArrayList<SpielModell>();
		this.context = context;
	}
	
	@Override
	public List<SpielModell> getGames() {
		return this.games;
	}

	@Override
	public int getCount() {
		return games.size();
	}

	@Override
	public Object getItem(int position) {
		return games.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.lobby_item_parent, parent, false);
		}

		TextView gameName = (TextView) convertView.findViewById(R.id.lobby_parent_spielname);
		gameName.setText(games.get(position).getName());

		TextView players = (TextView) convertView.findViewById(R.id.lobby_parent_beigetrene);
		players.setText(
				games.get(position).getSpieler().size()
				+ "/"
				+ games.get(position).getMaxSpielerAnzahl()
				+ " "
				+ context.getResources().getString(R.string.players)
				);

		Button joinButton = (Button) convertView.findViewById(R.id.lobby_parent_button);
		joinButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				joinGame(games.get(position));
			}
		});

		return convertView;
	}
	
	private void joinGame(final SpielModell spiel) {
		DominionApplication.getInstance().getManager().getSpieleVerwaltungService().meldeSpielerAn(spiel,
				DominionApplication.getInstance().getManager().getEigenerNutzer(),
				new AbstractCallback<SpielerModell>() {
					@Override
					public void onFailure(Throwable caught) {
						Log.e(TAG, "unable to join game " + spiel.getSpielID());
						Log.e(TAG, Log.getStackTraceString(caught));
					}

					@Override
					public void onSuccess(SpielerModell result) {
						ActivityNavigationManager navManager = 
								((LobbyActivity) context).getActivityNavigationManager();
						int position = DominionApplication.getInstance().
								getActivityNavigationAdapter().getNavPosition(spiel);
						navManager.onNavigationItemSelected(position, position);
					}
				});
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return games.isEmpty();
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		System.out.println("DATA SET CHANGED!");
	}

}
