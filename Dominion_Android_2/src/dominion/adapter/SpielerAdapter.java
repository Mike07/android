package dominion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.SpielModell;
import dominion.miniModell.modell.elements.SpielerModell;
import dominion.miniModell.modell.observer.addDelete.SpielerAddDeleteObserver;

public class SpielerAdapter extends BaseAdapter implements SpielerAddDeleteObserver {
	private List<SpielerModell> spieler;
	private LayoutInflater inflater;

	public SpielerAdapter(Context context, SpielModell spiel) {
		super();
		this.inflater = LayoutInflater.from(context);
		this.spieler = new ArrayList<SpielerModell>();

		spiel.addSpielerAddDeleteObserver(this, true);
	}

	@Override
	public int getCount() {
		return spieler.size();
	}

	@Override
	public Object getItem(int position) {
		return spieler.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = inflater.inflate(R.layout.vorbereitung_item_spieler, parent, false);

		ImageView i = (ImageView) v.findViewById(R.id.vorbereitung_item_bild);
		i.setImageBitmap(PictureLoader.getInstance().getAvatarGross(spieler.get(position).getNutzer()));

		TextView t = (TextView) v.findViewById(R.id.vorbereitung_item_name);
		SpielerModell sp = spieler.get(position);
		String spielerName = "";
		if (sp.getNutzer() == null) {
			if (sp.getKi() == null) {
				spielerName = "Nutzer+KI sind null!";
			} else {
				spielerName = sp.getKi().getName() + sp.getSpielerID();
			}
		} else {
			spielerName = sp.getNutzer().getNickname();
		}
		t.setText(spielerName);

		return v;
	}

	@Override
	public void addSpieler(SpielerModell spieler) {
		new AsyncTask<SpielerModell, Void, SpielerModell>() {
			@Override
			protected SpielerModell doInBackground(SpielerModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(SpielerModell result) {
				SpielerAdapter.this.spieler.add(result);
				SpielerAdapter.this.notifyDataSetChanged();
			}
		}.execute(spieler);
	}

	@Override
	public void deleteSpieler(SpielerModell spieler) {
		new AsyncTask<SpielerModell, Void, SpielerModell>() {
			@Override
			protected SpielerModell doInBackground(SpielerModell... params) {
				return params[0];
			}

			@Override
			protected void onPostExecute(SpielerModell result) {
				SpielerAdapter.this.spieler.remove(result);
				SpielerAdapter.this.notifyDataSetChanged();
			}
		}.execute(spieler);
	}
}
