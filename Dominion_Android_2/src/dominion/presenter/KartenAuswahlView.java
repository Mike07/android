package dominion.presenter;

import java.util.List;

import dominion.miniModell.modell.elements.KarteModell;

public interface KartenAuswahlView {
	public void starteAuswahl(KartenAuswahlStrg strg, List<KarteModell> waehlbare, int min, int max, boolean reihenfolgeWaehlbar);
	public void beendeAuswahl();
}
