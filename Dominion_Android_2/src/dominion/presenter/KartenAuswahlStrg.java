package dominion.presenter;

import java.util.List;

import dominion.miniModell.modell.elements.KarteModell;

public interface KartenAuswahlStrg {
	public void onAuswahlFertig(List<KarteModell> ausgewaehlte);
}
