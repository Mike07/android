package dominion.main;

import android.app.Activity;

public interface OnNewDeleteActivity {
	public void onNewActivity(Activity activity);
	public void onDeleteActivity(Activity activity);
}
