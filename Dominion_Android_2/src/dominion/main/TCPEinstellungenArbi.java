package dominion.main;

import dominion.miniModell.synchro.javaAndroid.TCPEinstellungen;

public class TCPEinstellungenArbi implements TCPEinstellungen {
	@Override
	public String getServerIP() {
		return "134.106.11.89"; // duemmer.informatik.uni-oldenburg.de
	}

	@Override
	public int getServerPort() {
		return 63180; // 43180 (Tomcat), 63180 (letzter freier)
	}

	@Override
	public int getServerMaxWaitTime() {
		return 7 * 24 * 60 * 60 * 1000;
	}
}
