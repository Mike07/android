package dominion.main;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.logging.Logger;

import dominion.DominionApplication;
import dominion.miniModell.ManagerClient;
import dominion.miniModell.klassifikation.callback.AbstractCallback;
import dominion.miniModell.modell.elements.NutzerModell;

public class ErrorHandler implements UncaughtExceptionHandler {
	private final static Logger logger = Logger.getLogger(ErrorHandler.class.getName());

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		logger.info("In Thread " + thread + " ist folgender Fehler aufgetreten:");
		ex.printStackTrace();
		logger.info("Im Folgenden wird versucht, ein möglicherweise 'geleertes' Modell wieder zu 'füllen'!");

//		this.abUndAnMelden();
		DominionApplication.getInstance().stopSynchro();
	}

	public void abUndAnMelden() {
		if (!DominionApplication.getInstance().istModellVorhanden()) {
			final ManagerClient manager = DominionApplication.getInstance().getManager();
			manager.getMitgliederService().beendeSitzung(manager.getEigenerNutzer(), new AbstractCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					logger.info("onFailure:");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(Void result) {
					logger.info("onSuccess: jetzt initSitzung()!");
					manager.getMitgliederService().initSitzung(new AbstractCallback<NutzerModell>() {
						@Override
						public void onFailure(Throwable caught) {
							logger.info("onFailure:");
							caught.printStackTrace();
						}

						@Override
						public void onSuccess(NutzerModell result) {
							logger.info("onSuccess:");
							if (DominionApplication.getInstance().istModellVorhanden()) {
								logger.info("Das Modell scheint wieder vorhanden zu sein!");
							} else {
								logger.info("Das Modell scheint weiterhin nicht vorhanden zu sein!!");
							}
						}
					});
				}
			});
		}		
	}
}
