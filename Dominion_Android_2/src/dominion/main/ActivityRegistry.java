package dominion.main;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import dominion.navigation.GlobalNavigationManager;

/**
 * Jede gestartete App muss sich an dieser Singleton-Klasse registrieren.
 * @author Johannes
 */
public class ActivityRegistry {
	private static ActivityRegistry singleton;
	private List<OnNewDeleteActivity> observer;
	private GlobalNavigationManager navigation;

	private ActivityRegistry() {
		super();
		observer = new ArrayList<OnNewDeleteActivity>();
		navigation = new GlobalNavigationManager();
		this.addNewDeleteActivityObserver(navigation);

		// TODO: Initialisierung des Modells (evtl. auch per Observer!)
	}

	public static ActivityRegistry getRegistry() {
		if (singleton == null) {
			singleton = new ActivityRegistry();
		}
		return singleton;
	}

	public void register(Activity activity) {
		for (OnNewDeleteActivity o : observer) {
			o.onNewActivity(activity);
		}
		// TODO: weiteres Verwalten?!
	}

	public void deregister(Activity activity) {
		for (OnNewDeleteActivity o : observer) {
			o.onDeleteActivity(activity);
		}
		// TODO: weiteres Verwalten?!		
	}

	public void addNewDeleteActivityObserver(OnNewDeleteActivity observer) {
		this.observer.add(observer);
	}

	public GlobalNavigationManager getNavigation() {
		return navigation;
	}
}
