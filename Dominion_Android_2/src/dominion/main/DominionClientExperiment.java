package dominion.main;

import java.util.logging.Logger;

import dominion.communication.util.ThreadUtilities;
import dominion.miniModell.ManagerClient;
import dominion.miniModell.ManagerClientImpl;
import dominion.miniModell.klassifikation.callback.AbstractCallback;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.synchro.javaAndroid.TCPAndroidClientToServer;

public class DominionClientExperiment {
	private final static Logger logger = Logger.getLogger(DominionClientExperiment.class.getName());
	private static DominionClientExperiment instance;

	private ManagerClient manager = null;
	private TCPAndroidClientToServer client;

	private DominionClientExperiment() {
		manager = ManagerClientImpl.initManager(null, null, null);

		this.connect();
	}

	public static DominionClientExperiment getInstance() {
		if (instance == null) {
			instance = new DominionClientExperiment();
		}
		return instance;
	}

	public void connect() {
		client = new TCPAndroidClientToServer(manager.getSynchronisierungPort(), new TCPEinstellungenArbi());
		client.init();
		manager.setServerMechanismus(client);

		ThreadUtilities.sleep(3 * 1000);

		// MUSS zu Beginn jeder Sitzung / Kommunikation mit dem Server aufgerufen werden!
		try {
			manager.getMitgliederService().initSitzung(new AbstractCallback<NutzerModell>() {
				@Override
				public void onFailure(Throwable caught) {
					caught.printStackTrace();
					logger.info("onFailure");
				}

				@Override
				public void onSuccess(NutzerModell result) {
					// in der Zwischenzeit ist das Modell schon angekommen!
					manager.setEigenerNutzer(result);
					logger.info("onSuccess: " + result.getNickname());
				}
			});
			logger.info("durchgelaufen!");
		} catch (NullPointerException npe) {
			logger.info("Server appears to be down.");
		}		
	}

	public void stop() {
		client.stop();
	}

	public ManagerClient getClient() {
		return manager;
	}

}
