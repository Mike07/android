package dominion.navigation;

import java.util.ArrayList;

import android.app.Activity;
import dominion.main.OnNewDeleteActivity;

/**
 * Speichert global alle Navigations-Einträge (wird von der ActivityRegistry benachrichtigt).
 * @author Johannes
 */
public class GlobalNavigationManager implements OnNewDeleteActivity {
	private ArrayList<NavigationEntry> entries;

	public GlobalNavigationManager() {
		entries = new ArrayList<NavigationEntry>();
	}

	@Override
	public void onNewActivity(Activity activity) {
		if (!(activity instanceof NavigationEntrySender)) {
			throw new ClassCastException("Activity muss NavigationEntrySender implementieren!");
		}
		NavigationEntrySender navi = (NavigationEntrySender) activity;
		if (!navi.useNavigation()) {
			return;
		}

		int index = isInList(navi);
		if (index >= 0) { // bereits in Liste drin
			if (navi.showAsEntry()) {
				// nichts tun
			} else {
				// TODO: aus der Liste nehmen
			}
		} else { // (noch) nicht in der Liste
			if (navi.showAsEntry()) {
				// TODO: in die Liste stecken
			} else {
				// nichts tun
			}
		}
	}

	@Override
	public void onDeleteActivity(Activity activity) {
		// TODO Auto-generated method stub
		// s.o.!!
	}

	private int isInList(NavigationEntrySender navi) {
		for (int i = 0; i < entries.size(); i++) {
//			if (activties.get(i).getActivityClass().equals(navi.getActivityClass())) {
//				return i;
//			}
		}
		return -1;
	}
}
