package dominion.navigation;

import android.app.ActionBar;
import android.content.Context;
import dominion.ActivityNavigationManager;

/**
 * Muss jede Activity implementieren, um an der Navigation per DropDown-Menü teilzunehmen oder diese anzuzeigen.
 * @author Johannes
 */
public interface NavigationEntrySender {
	/**
	 * String, der als Beschriftung dieser Activity dienen soll
	 * @return
	 */
	public String getNavigationName();
	/**
	 * Klassen-Objekt dieser Activity
	 * @return
	 */
	public Class<?> getActivityClass();

	/**
	 * Verwendet diese Activity überhaupt die Navigation?
	 * @return
	 */
	public boolean useNavigation();
	/**
	 * Gibt an, ob diese Activity ab jetzt sichtbar (und navigierbar) oder nicht sichtbar sein soll.
	 * @return
	 */
	public boolean showAsEntry();

	/**
	 * Setzt die ID dieser Activity im Navigation-Menü (evtl. etwas unschön gelöst)
	 * @param id
	 */
	public void setNavigationID(int id);
	/**
	 * Liefert die ID dieser Activity im Navigation-Menü (evtl. etwas unschön gelöst)
	 * @return
	 */
	public int getNavigationID();

	public ActivityNavigationManager getActivityManager();

	public ActionBar getActionBar();
	public Context getActionBarThemedContextCompat();
}
