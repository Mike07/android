package dominion.navigation;

public class NavigationEntry {
	private Class<?> activityClass;
	private String entryName;

	public NavigationEntry(Class<?> activityClass, String entryName) {
		super();
		this.activityClass = activityClass;
		this.entryName = entryName;
	}

	public Class<?> getActivityClass() {
		return activityClass;
	}

	public String getEntryName() {
		return entryName;
	}
}
