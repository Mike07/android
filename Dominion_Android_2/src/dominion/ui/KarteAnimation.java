package dominion.ui;

import dominion.miniModell.modell.elements.KarteModell;
import dominion.miniModell.modell.elements.KartenTypModell;

/**
 * Interface, das das Animieren von Karten anbietet.
 * @author Johannes
 */
public interface KarteAnimation {
	/**
	 * Zeigt die übergebene Karte in Großansicht dar.
	 * @param karte Ist karte null, wird keine Karte angezeigt
	 */
	public void showKarteBig(KarteModell karte);

	/**
	 * Zeigt den übergebenen KartenTyp in Großansicht dar.
	 * @param typ Ist typ null, wird nichts angezeigt
	 */
	public void showKartenTypBig(KartenTypModell typ);
}
