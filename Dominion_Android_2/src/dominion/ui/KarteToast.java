package dominion.ui;

import android.content.Context;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;
import dominion.data.PictureLoader;
import dominion.miniModell.modell.elements.KartenTypModell;

/**
 * Klasse, um eine Karte kurzzeitig als Toast darzustellen
 * @author Johannes
 */
public class KarteToast extends Toast {
	private KarteToast(Context context) {
		super(context);
	}

	public static void showKarteToast(Context context, KartenTypModell kartenTyp) {
		ImageView v = new ImageView(context);
		v.setImageBitmap(PictureLoader.getInstance().getBildGross(kartenTyp));

		KarteToast toast = new KarteToast(context);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(v);
		toast.show();
	}
}
