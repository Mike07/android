package dominion.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import dominion.adapter.KartenAdapter;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.presenter.KartenAuswahlStrg;
import dominion.presenter.KartenAuswahlView;

/**
 * View zum zeilen-weisen Darstellen von Karten
 * - Reaktion auf OnTouch der gesamten View eingebaut (vor den Kindern)
 * - Verwaltet Karten-Animationen für die einzelnen enthaltenen Karten.
 * @author Johannes
 */
public class KartenAuslageView extends HorizontalListView implements KartenAuswahlView {
    private OnTouchedKartenAuslageView ownHandler;
    private ArrayList<KarteModell> karten;
	private KarteAnimation animationKarte;
	private KartenAdapter adapter;

	private KartenAuswahlStrg strg;
	private List<KarteModell> waehlbare;
	private int min;
	private int max;
	private boolean reihenfolgeWaehlbar;
	private boolean auswaehlen;

	public KartenAuslageView(Context context) {
		super(context);
		this.initView();
	}

	public KartenAuslageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initView();
		this.initView(context, attrs);
	}

    public KartenAuslageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.initView();
		this.initView(context, attrs);
	}

    private void initView(Context context, AttributeSet attrs) {
    	TypedArray arr = context.getTheme().obtainStyledAttributes(attrs, R.styleable.KartenAuslageView, 0, 0);
    	try {
    		this.setRowsCount(arr.getInteger(R.styleable.KartenAuslageView_rowsCount, 1));
    	} finally {
    		arr.recycle();
    	}
    }

    private void initView() {
    	this.ownHandler = null;
    	this.karten = new ArrayList<KarteModell>();
    	this.animationKarte = null;
    	this.adapter = new KartenAdapter();

    	this.adapter.setKarten(karten);
    	this.adapter.setAnimation(animationKarte);
    	this.setAdapter(this.adapter);

    	this.auswaehlen = false;
    }

    @Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
    	// eher unschöne Lösung:
		if (ownHandler != null) {
			ownHandler.onTouchedKartenAuslageView(this);
		}
		return super.dispatchTouchEvent(ev);
	}

	public OnTouchedKartenAuslageView getOwnHandler() {
		return ownHandler;
	}

	public void setOwnHandler(OnTouchedKartenAuslageView ownHandler) {
		this.ownHandler = ownHandler;
	}

	public List<KarteModell> getKarten() {
		return karten;
	}

	public void setKarten(ArrayList<KarteModell> karten) {
		this.karten = karten;
		this.adapter.setKarten(this.karten);
	}

	public KarteAnimation getAnimationKarte() {
		return animationKarte;
	}

	public void setAnimationKarte(KarteAnimation animationKarte) {
		this.animationKarte = animationKarte;
		this.adapter.setAnimation(this.animationKarte);
	}

	public int getRowsCount() {
		return this.adapter.getRowsCount();
	}

	public void setRowsCount(int rowsCount) {
		this.adapter.setRowsCount(rowsCount);
		// Layout neu setzen
		invalidate();
		requestLayout();
	}

	@Override
	public void starteAuswahl(KartenAuswahlStrg strg, List<KarteModell> waehlbare, int min, int max, boolean reihenfolgeWaehlbar) {
		this.strg = strg;
		this.auswaehlen = true;

		this.waehlbare = waehlbare;
		this.min = min;
		this.max = max;
		this.reihenfolgeWaehlbar = reihenfolgeWaehlbar;

		// TODO Auto-generated method stub
		this.adapter.starteAuswahl(strg, waehlbare, min, max, reihenfolgeWaehlbar);
	}

	@Override
	public void beendeAuswahl() {
		// TODO Auto-generated method stub
		this.adapter.beendeAuswahl();
	}
}
