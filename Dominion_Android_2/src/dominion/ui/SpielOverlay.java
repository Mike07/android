package dominion.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.miniModell.modell.elements.KartenTypModell;

/**
 * Meist unsichtbares / durchscheinendes Overlay über das gesamte Spielfeld,
 * das u.a. für die Großansicht von Karten genutzt wird.
 * @author Johannes
 */
public class SpielOverlay extends ImageView implements KarteAnimation {
	private Resources res;

	private Bitmap bitmap;
	private Canvas canvas;
	private Paint paint;

	private KarteModell karte;
	private KartenTypModell typ;
	private boolean showBigKarte;
	private Bitmap karteBitmap;

	private int xPos; // Mittelpunkt des Bildes
	private int yPos;

	private boolean downMove;
	private float downX;
	private float downY;

	public SpielOverlay(Context context) {
		super(context);
		this.initView(context);		
	}

	public SpielOverlay(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initView(context);
	}

	public SpielOverlay(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.initView(context);
	}

	private void initView(Context context) {
		res = context.getResources();
		karte = null;
		typ = null;
		showBigKarte = false;
		karteBitmap = null;

		xPos = this.getWidth() / 2;
		yPos = this.getHeight() / 2;

		downMove = false;
		downX = 0;
		downY = 0;

		this.initImage();
		this.renderImageNew();

		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (showBigKarte) {
					if (Math.abs(event.getX() - xPos) <= karteBitmap.getWidth() / 2 &&
							Math.abs(event.getY() - yPos) <= karteBitmap.getHeight() / 2) {
						// Klick hat direkt das Bild getroffen:
						if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
							downMove = true;
							downX = event.getX();
							downY = event.getY();
						}
						if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
							xPos += Math.round(event.getX() - downX);
							xPos = Math.min(xPos, SpielOverlay.this.getWidth() - SpielOverlay.this.karteBitmap.getWidth() / 2);
							xPos = Math.max(xPos, SpielOverlay.this.karteBitmap.getWidth() / 2);

							yPos += Math.round(event.getY() - downY);
							yPos = Math.min(yPos, SpielOverlay.this.getHeight() - SpielOverlay.this.karteBitmap.getHeight() / 2);
							yPos = Math.max(yPos, SpielOverlay.this.karteBitmap.getHeight() / 2);

							SpielOverlay.this.renderImageNew();
							downX = event.getX();
							downY = event.getY();
						}
						if (event.getActionMasked() == MotionEvent.ACTION_HOVER_EXIT && downMove) {
							downMove = false;
						}
						if (event.getActionMasked() == MotionEvent.ACTION_CANCEL && downMove) {
							downMove = false;
						}
						if (event.getActionMasked() == MotionEvent.ACTION_OUTSIDE && downMove) {
							downMove = false;
						}
						if (event.getActionMasked() == MotionEvent.ACTION_UP && downMove) {
							downMove = false;
						}
						return true;
					} else {
						// Klick außerhalb des Bildes:
						SpielOverlay.this.showBigKarte = false;
						SpielOverlay.this.renderImageNew();
						return false;
					}
				}
				return false;
			}
		});
	}

	private void initImage() {
		paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTextAlign(Align.CENTER);

		bitmap = Bitmap.createBitmap(Math.max(this.getWidth(), R.integer.spiel_overlay_mindest_groesse),
				Math.max(this.getHeight(), R.integer.spiel_overlay_mindest_groesse), Bitmap.Config.ARGB_8888);

		canvas = new Canvas();
		canvas.setBitmap(bitmap);

		this.setImageBitmap(bitmap);
	}

	/**
	 * Leert das Bild (d.h. komplett durchscheinend), entfernt alle Handler.
	 */
	private void renderClearImage() {
		showBigKarte = false;
		this.renderImageNew();
	}

	/**
	 * Zeichnet das Bild komplett neu anhand der Angaben in den privaten globalen Variablen!
	 */
	private void renderImageNew() {
		this.initImage();

		if (showBigKarte && (karte != null || typ != null) && karteBitmap != null) {
			canvas.drawBitmap(karteBitmap, xPos - karteBitmap.getWidth() / 2, yPos - karteBitmap.getHeight() / 2, paint);
		}
	}

	@Override
	public void showKarteBig(KarteModell karte) {
		if (karte == null) {
			this.renderClearImage();
		} else {
			System.out.println("SpielOverlay: gesetzt!!!!!!!!!!!!"); // es funktioniert also durchaus!
			this.karte = karte;
			this.showBigKarte = true;
			karteBitmap = PictureLoader.getInstance().getBild(karte.getKartenTyp());
			this.renderImageNew();
		}
	}

	@Override
	public void showKartenTypBig(KartenTypModell typ) {
		if (typ == null) {
			this.renderClearImage();
		} else {
			System.out.println("SpielOverlay: gesetzt!!!!!!!!!!!!"); // es funktioniert also durchaus!
			this.typ = typ;
			this.showBigKarte = true;
			karteBitmap = PictureLoader.getInstance().getBild(typ);
			this.renderImageNew();
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		this.initImage();
		// TODO: Positionsangaben skalieren beim Ändern der Größe?! Drehen des Devices??
		// initial: zentriert
		xPos = w / 2;
		yPos = h / 2;
		this.renderImageNew();
	}
}
