package dominion.ui;

/**
 * Beobachter-Interface, das über das Berühren einer KartenAuslageView benachrichtigt wird.
 * @author Johannes
 */
public interface OnTouchedKartenAuslageView {
	public void onTouchedKartenAuslageView(KartenAuslageView view);
}
