package dominion.ui.karte;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import dominion.DominionApplication;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.miniModell.modell.elements.SpielerModell;

/**
 * View, die eine einzelne Karte anzeigt.
 * @author Johannes
 */
public class KarteView extends AbstractKarteImageView {
	private KarteModell karte;

	public KarteView(Context context) {
		super(context);
		this.initView(context);
	}

	public KarteView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initView(context);
	}

	public KarteView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.initView(context);
	}

	protected void initView(Context context) {
		super.initView(context);

		karte = null;

		this.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (karte != null && karte.siehtSpielerKarte(KarteView.this.getEigenerSpieler())) {
					if (animation != null) { // z.B. im Shop nicht (ein)gesetzt
						animation.showKarteBig(karte);
					}
					return true;
				}
				return false;
			}
		});

		this.renderView();
	}

	private SpielerModell getEigenerSpieler() {
		for (SpielerModell sp : this.karte.getSpiel().getSpieler()) {
			if (sp.getNutzer().equals(DominionApplication.getInstance().getManager().getEigenerNutzer())) {
				return sp;
			}
		}
		return null;
	}

	@Override
	protected boolean drawKarte() {
		if (karte != null) {
			if (karte.siehtSpielerKarte(this.getEigenerSpieler())) { // Vorderseite sichtbar
				canvas.drawBitmap(PictureLoader.getInstance().getBild(karte.getKartenTyp()),
						0, 12.0f, paint);
				canvas.drawBitmap(BitmapFactory.decodeResource(res, R.drawable.aktion_template), 0, 0, paint);

				paint.setTextSize(res.getInteger(R.integer.karte_schrift_name));
				// TODO proper error handling
				String text = karte.getKartenTyp() != null ? karte.getKartenTyp().getName() : "NULL";
				canvas.drawText(text, res.getInteger(R.integer.karte_schrift_x),
						res.getInteger(R.integer.karte_schrift_y), paint);

				paint.setColor(Color.BLACK);
				paint.setTextSize(res.getInteger(R.integer.karte_schrift_kosten));
				// TODO proper error handling
				int münzen = karte.getKartenTyp() != null ? karte.getKartenTyp().getKosten().getMuenzen() : 2;
				canvas.drawText(münzen + "",
						res.getInteger(R.integer.karte_kosten_x),
						res.getInteger(R.integer.karte_kosten_y), paint);
			} else { // Rückseite sichtbar
				canvas.drawBitmap(BitmapFactory.decodeResource(res, R.drawable.rueckseite), 0, 0, paint);				
				canvas.drawBitmap(BitmapFactory.decodeResource(res, R.drawable.rueckseite_template), 0, 0, paint);				
			}
		}
		return (karte != null);
	}

	public KarteModell getKarte() {
		return karte;
	}

	public void setKarte(KarteModell karte) {
		this.karte = karte;
		this.renderView();
	}
}
