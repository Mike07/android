package dominion.ui.karte;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import dominion.dominion_android_2.R;
import dominion.ui.KarteAnimation;

/**
 * ImageView, um damit bequem Karten in verschiedenen Zuständen anzuzeigen und Karten zu vergrößern / zu animieren.
 * @author Johannes
 */
//Idee: http://www.milestone-blog.de/android-development/einfaches-zeichnen-canvas/
public abstract class AbstractKarteImageView extends ImageView {
	public final static int KARTE_PADDING = 4;
	public final static int CHECKED_NUMBER_NOTHING = -1;

	private Bitmap bitmap;
	protected Canvas canvas;
	protected Paint paint;

	protected Resources res;

	protected KarteAnimation animation;

	private boolean aktiv;
	private boolean countNumbers;
	private int countNumber;
	private boolean checked;
	private int checkedNumber;

	public AbstractKarteImageView(Context context) {
		super(context);
		this.initView(context);
	}

	public AbstractKarteImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initView(context);
	}

	public AbstractKarteImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.initView(context);
	}

	protected void initView(Context context) {
		res = context.getResources();

		this.aktiv = true;
		this.countNumbers = false;
		this.checked = false;
		this.checkedNumber = AbstractKarteImageView.CHECKED_NUMBER_NOTHING;

		int pad = AbstractKarteImageView.KARTE_PADDING;
		this.setPadding(pad, pad, pad, pad);
		this.setClickable(true);

		paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTextAlign(Align.CENTER);
		paint.setTypeface(Typeface.DEFAULT_BOLD);

		bitmap = Bitmap.createBitmap(res.getInteger(R.integer.karte_groesse), res.getInteger(R.integer.karte_groesse), Bitmap.Config.ARGB_8888);

		canvas = new Canvas();
		canvas.setBitmap(bitmap);

		this.renderView();
	}

	/**
	 * Zeichnet die Karte
	 * @return true wenn eine Karte gezeichnet wurde, sonst false
	 */
	protected abstract boolean drawKarte();

	protected final void renderView() {
		paint.setColor(res.getColor(R.color.karte_leerer_untergrund));
		canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);
		paint.setStyle(Style.FILL_AND_STROKE);

		// TODO: einige eckige Ecken abrunden -> Graphiken austauschen!

		paint.setColor(Color.BLACK);
		if (this.drawKarte()) {
			if (this.isChecked()) { // Auswahl-Haken anzeigen (auch auf der Rückseite!)
				canvas.drawBitmap(BitmapFactory.decodeResource(res, R.drawable.checked_round),
						res.getInteger(R.integer.karte_checked_x), res.getInteger(R.integer.karte_checked_y), paint);
			}

			if (this.isCountNumbers()) {
				int x = res.getInteger(R.integer.karte_count_x);
				int y = res.getInteger(R.integer.karte_count_y);
				int rect = res.getInteger(R.integer.karte_count_size);

				// Fläche füllen
				paint.setColor(res.getColor(R.color.karte_count_fill));
				paint.setStyle(Style.FILL);
				canvas.drawRect(x - rect, y - rect, x + rect, y + rect, paint);

				// Rahmen der Fläche zeichnen
				paint.setColor(res.getColor(R.color.karte_count_draw));
				paint.setStyle(Style.STROKE);
				paint.setStrokeWidth(res.getInteger(R.integer.karte_count_stroke_width));
				canvas.drawRect(x - rect, y - rect, x + rect, y + rect, paint);
				paint.setStrokeWidth(0);

				// Anzahl hinschreiben
				paint.setTextSize(res.getInteger(R.integer.karte_schrift_count_number));
				canvas.drawText(this.getCountNumber() + "", x + res.getInteger(R.integer.karte_count_add_text_x),
						y + res.getInteger(R.integer.karte_count_add_text_y), paint);
			}

			// Auswahl-Nummern anzeigen
			if (this.getCheckedNumber() != AbstractKarteImageView.CHECKED_NUMBER_NOTHING) {
				canvas.drawBitmap(BitmapFactory.decodeResource(res, R.drawable.checked_number),
						res.getInteger(R.integer.karte_checked_x), res.getInteger(R.integer.karte_checked_y), paint);
				paint.setColor(Color.WHITE);
				paint.setTextSize(res.getInteger(R.integer.karte_schrift_check_number));
				canvas.drawText(this.getCheckedNumber() + "", res.getInteger(R.integer.karte_checked_text_x),
						res.getInteger(R.integer.karte_checked_text_y), paint);
			}

			// Karte deaktivieren
			if (!this.isAktiv()) {
				paint.setColor(res.getColor(R.color.karte_inactive));
				canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);
			}
		}

		this.setImageBitmap(bitmap);
	}

	public final void setAnimation(KarteAnimation animation) {
		this.animation = animation;
	}

	public final boolean isAktiv() {
		return aktiv;
	}

	public final void setAktiv(boolean aktiv) {
		this.aktiv = aktiv;
		this.invalidate();
	}

	public final boolean isCountNumbers() {
		return countNumbers;
	}

	public final void setCountNumbers(boolean countNumbers) {
		this.countNumbers = countNumbers;
		this.invalidate();
	}

	public final int getCountNumber() {
		return countNumber;
	}

	public final void setCountNumber(int countNumber) {
		this.countNumber = countNumber;
		this.invalidate();
	}

	public final boolean isChecked() {
		return checked;
	}

	public final void setChecked(boolean checked) {
		this.checked = checked;
		this.invalidate();
	}

	public final int getCheckedNumber() {
		return checkedNumber;
	}

	public final void setCheckedNumber(int checkedNumber) {
		this.checkedNumber = checkedNumber;
		this.invalidate();
	}
}
