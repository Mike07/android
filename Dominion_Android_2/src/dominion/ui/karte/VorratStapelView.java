package dominion.ui.karte;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import dominion.data.PictureLoader;
import dominion.dominion_android_2.R;
import dominion.miniModell.modell.elements.KartenStapelModell;

/**
 * View, die einen einzelnen Vorratsstapel anzeigt.
 * @author Johannes
 */
public class VorratStapelView extends AbstractKarteImageView {
	private KartenStapelModell stapel;

	public VorratStapelView(Context context) {
		super(context);
		this.initView(context);
	}

	public VorratStapelView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initView(context);
	}

	public VorratStapelView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.initView(context);
	}

	protected void initView(Context context) {
		super.initView(context);

		stapel = null;
		this.setCountNumbers(true);
		this.setCountNumber(0);

		this.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (stapel != null && stapel.getAnzahl() > 0) {
					if (animation != null) { // z.B. im Shop nicht (ein)gesetzt
						animation.showKartenTypBig(stapel.getPlatzhalterKartenTyp());
					}
					return true;
				}
				return false;
			}
		});

		this.renderView();
	}

	@Override
	protected boolean drawKarte() {
		if (stapel != null) {
			canvas.drawBitmap(PictureLoader.getInstance().getBild(stapel.getPlatzhalterKartenTyp()),
					0, 12.0f, paint);
			canvas.drawBitmap(BitmapFactory.decodeResource(res, R.drawable.aktion_template), 0, 0, paint);

			paint.setTextSize(res.getInteger(R.integer.karte_schrift_name));
			canvas.drawText(stapel.getPlatzhalterKartenTyp().getName(), res.getInteger(R.integer.karte_schrift_x),
					res.getInteger(R.integer.karte_schrift_y), paint);

			paint.setColor(Color.BLACK);
			paint.setTextSize(res.getInteger(R.integer.karte_schrift_kosten));
			canvas.drawText(stapel.getPlatzhalterKartenTyp().getKosten().getMuenzen() + "",
					res.getInteger(R.integer.karte_kosten_x),
					res.getInteger(R.integer.karte_kosten_y), paint);
		}
		return (stapel != null);
	}

	public void setVorratStapel(KartenStapelModell vorratStapel) {
		this.stapel = vorratStapel;
		this.setCountNumber(this.stapel.getAnzahl());
		this.renderView();
	}
}
