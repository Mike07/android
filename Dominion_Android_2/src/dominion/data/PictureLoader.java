package dominion.data;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Locale;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import dominion.DominionApplication;
import dominion.miniModell.modell.elements.KarteModell;
import dominion.miniModell.modell.elements.KartenTypModell;
import dominion.miniModell.modell.elements.NutzerModell;
import dominion.miniModell.modell.kommunikation.KarteInfo;

/**
 * Vereinfacht das Laden von Bildern.
 * Vereinfacht das Erstellen einzelner Karten.
 * @author Johannes, Klaas
 */
public class PictureLoader {
	public static final String ZEILENUMBRUCH = "\n";
	private static PictureLoader instance;
	private Resources res;

	private PictureLoader(Resources res) {
		this.res = res;
	}

	public static void init(Resources res) {
		instance = new PictureLoader(res);
	}

	public static PictureLoader getInstance() {
		return instance;
	}

	public KarteModell createDorf() {
		return new KarteModell(new KarteInfo(BigInteger.valueOf(0).toString(), 0, 0, new ArrayList<Integer>()),
				DominionApplication.getInstance().getManager().getModell());
	}

	public KarteModell createRatsversammlung() {
		return new KarteModell(new KarteInfo(BigInteger.valueOf(0).toString(), 1, 1, new ArrayList<Integer>()),
				DominionApplication.getInstance().getManager().getModell());
	}

	public KarteModell createBibliothek() {
		return new KarteModell(new KarteInfo(BigInteger.valueOf(0).toString(), 2, 2, new ArrayList<Integer>()),
				DominionApplication.getInstance().getManager().getModell());
	}

	public Bitmap getBild(KartenTypModell type) {
		String typeName;
		if (type != null) {
			typeName = type.getName().replace(' ', '_');
			typeName = typeName.toLowerCase(Locale.GERMANY);
		} else {
			typeName = "dorf";
		}
//		int resID = res.getIdentifier(type.getName(), "drawable", "dominion.dominion_android_2");
//		return BitmapFactory.decodeResource(res, resID);
		try {
			return BitmapFactory.decodeStream(res.getAssets().open("cards/normal/" + typeName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Bitmap getBildGross(KartenTypModell type) {
		String typeName;
		if (type != null) {
			typeName = type.getName().replace(' ', '_');
			typeName = typeName.toLowerCase(Locale.GERMANY);
		} else {
			typeName = "dorf";
		}
//		typeName += "_gross";
//		int resID = res.getIdentifier(typeName, "drawable", "dominion.dominion_android_2");
//		return BitmapFactory.decodeResource(res, resID);
		try {
			return BitmapFactory.decodeStream(res.getAssets().open("cards/big/" + typeName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Bitmap getAvatarGross(NutzerModell nutzer) {
		String typeName;
		if (nutzer != null) {
			typeName = nutzer.getBild();
		} else {
			typeName = null;
		}
		if (typeName == null || typeName.isEmpty())
			typeName = "01";
		try {
			return BitmapFactory.decodeStream(res.getAssets().open("avatars/normal/" + typeName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}	

	public Bitmap getAvatarKlein(NutzerModell nutzer) {
		String typeName;
		if (nutzer != null) {
			typeName = nutzer.getBild();
		} else {
			typeName = null;
		}
		if (typeName == null || typeName.isEmpty())
			typeName = "01";
		try {
			return BitmapFactory.decodeStream(res.getAssets().open("avatars/small/" + typeName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Bitmap getLogo() {
		try {
			return BitmapFactory.decodeStream(res.getAssets().open("dominion_logo_title.png"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
