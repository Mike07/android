package com.example.testapp1;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ExpandableListView;

import com.example.testapp1.own.MyCustomAdapter;

public class ShopActivity extends Activity {
	
	public class Parent {
		private String mTitle;
		private ArrayList<String> mArrayChildren;

		public String getTitle() {
			return mTitle;
		}

		public void setTitle(String mTitle) {
			this.mTitle = mTitle;
		}

		public ArrayList<String> getArrayChildren() {
			return mArrayChildren;
		}

		public void setArrayChildren(ArrayList<String> mArrayChildren) {
			this.mArrayChildren = mArrayChildren;
		}
	}
	
	private ExpandableListView mExpandableList;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop);

		mExpandableList = (ExpandableListView) findViewById(R.id.expandableListView1);

		ArrayList<Parent> arrayParents = new ArrayList<Parent>();
		ArrayList<String> arrayChildren = new ArrayList<String>();

		// here we set the parents and the children
		for (int i = 0; i < 10; i++) {
			// for each "i" create a new Parent object to set the title and the
			// children
			Parent parent = new Parent();
			parent.setTitle("Parent " + i);

			arrayChildren = new ArrayList<String>();
			for (int j = 0; j < 10; j++) {
				arrayChildren.add("Child " + j);
			}
			parent.setArrayChildren(arrayChildren);

			// in this array we add the Parent object. We will use the
			// arrayParents at the setAdapter
			arrayParents.add(parent);
		}

		// sets the adapter that provides data to the list.
		mExpandableList.setAdapter(new MyCustomAdapter(ShopActivity.this,
				arrayParents));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shop, menu);
		return true;
	}

}
