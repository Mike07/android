package com.example.testapp1;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class SpielErstellenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spiel_erstellen);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.spiel_erstellen, menu);
		return true;
	}

}
