package com.example.testdropdown1;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

public class StartActivity extends Activity implements ActionBar.OnNavigationListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		// Set up the action bar to show a dropdown list.
		final ActionBar bar = getActionBar();
		bar.setDisplayShowTitleEnabled(false);
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		SpinnerAdapter ad = ArrayAdapter.createFromResource(this, R.array.drop_down_string_list,
				android.R.layout.simple_spinner_dropdown_item);

		SpinnerAdapter old = new ArrayAdapter<String>(getActionBarThemedContextCompat(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1, new String[] {
						getString(R.string.title_section1),
						getString(R.string.title_section2),
						getString(R.string.title_section3), });

		bar.setListNavigationCallbacks(ad, this);

//		System.out.println(bar.getNavigationItemCount());
//		bar.setSelectedNavigationItem(1); // funktioniert so wie gedacht!
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
		if (position > 0) {
			Intent intent = new Intent(this, SecondActivity.class);
			startActivity(intent);
		}
		return true;
	}
}
